CREATE TABLE `Utilisateur` 
(
	`id` int,
	`adresseMail` varchar(255),
	`password` varchar(255),
	`name` varchar(255),
	`company` varchar(255),
	`phoneNumber` varchar(255),
	`creationDate` DateTime,
	`status` bool,
	`admin` bool
);

CREATE TABLE `Sujet` 
(
	`id` int,
	`label` varchar(255)
);

CREATE TABLE `Questionnaire` 
(
	`id` int,
	`status` bool,
	`idSujet` int
);

CREATE TABLE `Question` 
(
	`id` int,
	`status` bool,
	`enonce` varchar(255),
	`idRep` int
);

CREATE TABLE `OrdreQuestion` 
(
	`idQuestionnaire` int,
	`idQuestion` int,
	`num` int
);

CREATE TABLE `Reponse` 
(
	`id` int,
	`texte` varchar(255),
	`status` bool
);

CREATE TABLE `OrdreReponse` 
(
	`idQuestion` int,
	`idReponse` int,
	`num` int
);

CREATE TABLE `Parcours` 
(
	`id` int,
	`score` int,
	`duree` int,
	`idUser` int,
	`idQuestionnaire` int
);

CREATE TABLE `Choix` 
(
	`idParcours` int,
	`idQuestion` int,
	`idChoix` int
);

ALTER TABLE `Questionnaire` ADD FOREIGN KEY (`idSujet`) REFERENCES `Sujet` (`id`);

ALTER TABLE `OrdreQuestion` ADD FOREIGN KEY (`idQuestionnaire`) REFERENCES `Questionnaire` (`id`);

ALTER TABLE `OrdreQuestion` ADD FOREIGN KEY (`idQuestion`) REFERENCES `Question` (`id`);

ALTER TABLE `OrdreReponse` ADD FOREIGN KEY (`idQuestion`) REFERENCES `Question` (`id`);

ALTER TABLE `OrdreReponse` ADD FOREIGN KEY (`idReponse`) REFERENCES `Reponse` (`id`);

ALTER TABLE `Parcours` ADD FOREIGN KEY (`idUser`) REFERENCES `Utilisateur` (`id`);

ALTER TABLE `Parcours` ADD FOREIGN KEY (`idQuestionnaire`) REFERENCES `Questionnaire` (`id`);

ALTER TABLE `Choix` ADD FOREIGN KEY (`idParcours`) REFERENCES `Parcours` (`id`);

ALTER TABLE `Choix` ADD FOREIGN KEY (`idQuestion`) REFERENCES `Question` (`id`);
