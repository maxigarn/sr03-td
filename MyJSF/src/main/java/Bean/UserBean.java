package Bean;

import DAO.DAOFactory;
import DAO.UserDao;
import Model.MailSender;
import Model.User;
import net.bootsfaces.utils.FacesMessages;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

/**
 * Bean des utilisateurs
 */
@Named
@ViewScoped
public class UserBean implements Serializable {

    private static final long serialVersionUID = 1;
    private List<User> allUsers;
    private List<User> allActiveUsers;
    private List<User> allInactiveUsers;
    private User user;
    private UserDao userDao;
    private boolean envoyer;

    @PostConstruct
    public void init(){
        envoyer = true;
        user = new User();
        DAOFactory dao = DAOFactory.getInstance();
        userDao = dao.getUserDao();
        allUsers = userDao.getAll();
        allActiveUsers = userDao.getByReq(userDao.sqlActiveUsers);
        allInactiveUsers = userDao.getByReq(userDao.sqlInactiveUsers);
    }

    /**
     * Enregistrement d'un utilisateur en base de données & Envoi du mail de confirmation
     */
    public void envoyer(){
        userDao.save(user);
        String content = "Votre compte a été créé avec succès sur la plateforme de quiz SR03"
                +"\n\nLogin: " + user.getEmail();
        MailSender.envoyerMailSMTP(user.getEmail(),content,true);
        envoyer = true;
        user = new User();
        FacesMessages.info("form:send","Info", "Utilisateur créé");
    }

    /**
     * Inversion du statut d'un utilisateur
     * @param user Utilisateur concerné
     */
    public void toggleStatus(User user){
        // Changement du status en base de données
        userDao.toggleStatus(user);
        // Rechargement des données utilisateurs
        allActiveUsers = userDao.getByReq(userDao.sqlActiveUsers);
        allInactiveUsers = userDao.getByReq(userDao.sqlInactiveUsers);
        FacesMessage message = new FacesMessage( "User : " + user.getLastName() + " " + user.getFirstName() + " changement de statut" );
        FacesContext.getCurrentInstance().addMessage( null, message );
    }

    // #####################
    // # Getters & Setters #
    // #####################

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<User> getAllUsers() {
        return allUsers;
    }

    public void setAllUsers(List<User> allUsers) {
        this.allUsers = allUsers;
    }

    public List<User> getAllActiveUsers() {
        return allActiveUsers;
    }

    public void setAllActiveUsers(List<User> allActiveUsers) {
        this.allActiveUsers = allActiveUsers;
    }

    public List<User> getAllInactiveUsers() {
        return allInactiveUsers;
    }

    public void setAllInactiveUsers(List<User> allInactiveUsers) {
        this.allInactiveUsers = allInactiveUsers;
    }

    public boolean isEnvoyer() {
        return envoyer;
    }

    public void setEnvoyer(boolean envoyer) {
        this.envoyer = envoyer;
    }
}
