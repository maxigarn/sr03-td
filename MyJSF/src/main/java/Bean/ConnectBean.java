package Bean;

import DAO.DAOFactory;
import DAO.UserDao;
import Model.User;
import net.bootsfaces.utils.FacesMessages;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

/**
 * Bean de connection
 */
@Named
@ManagedBean
@ViewScoped
public class ConnectBean implements Serializable {

    @Inject
    SessionBean sessionBean;
    private String mail;
    private String password;
    private UserDao userDao;

    @PostConstruct
    public void init(){
        DAOFactory dao = DAOFactory.getInstance();
        userDao = dao.getUserDao();
    }

    /**
     * Methode de connection
     * @return Page vers laquelle sera redirigé l'utilisateur
     */
    public String connect(){
        User usr = userDao.tryConnect(mail, password);
        FacesContext context = FacesContext.getCurrentInstance();


        if(usr != null && usr.getStatus()){
            sessionBean.setUser(usr);

            if (usr.getAdmin()){
                return "/admin/index.jsf";
            } else {
                return "/user/index.jsf";
            }
        }
        else {
            System.out.println("Connection failed");
            if (usr != null){
                FacesMessages.error("form:send","Connection failed!", "Votre compte est désactivé");
            } else {
                FacesMessages.error("form:send","Connection failed!", "Utilisateur ou mot de passe incorrect");
            }
            return "";
        }

    }

    // #####################
    // # Getters & Setters #
    // #####################

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        System.out.println("Set mail");
        this.mail = mail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        System.out.println("Set password");
        this.password = password;
    }

    public void setSessionBean(SessionBean sessionBean) {
        this.sessionBean = sessionBean;
    }
}
