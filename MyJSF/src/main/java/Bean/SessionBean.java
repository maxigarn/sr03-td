package Bean;

import Model.User;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;

@Named
//@ManagedBean
@SessionScoped
public class SessionBean implements Serializable {

    ExternalContext context;
    private User user;

    @PostConstruct
    public void init(){
        if(FacesContext.getCurrentInstance()!=null){
            FacesContext.getCurrentInstance().getViewRoot().getViewMap().remove(this);
            context = FacesContext.getCurrentInstance().getExternalContext();
        }
    }

    public void reset(){
        user = null;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void checkConnected() {

        if (user == null) {
            try {
                context.redirect(context.getRequestContextPath() + "/connect.jsf");
                return;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void checkAdmin() {
        if (user == null || !user.getAdmin()) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect(context.getRequestContextPath() + "/forbidden.jsf");
                return;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
