package Bean;

import Model.User;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Filtre d'accès aux différentes pages.
 * Permet de forcer la connexion et
 * de restreindre l'accès aux pages suivant les droits d'un utilisateur.
 */
public class AccessFilter implements Filter {

    @Inject
    @SessionScoped
    SessionBean sessionBean;

    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {

        User utilisateur = null;
        if (sessionBean!=null){
            utilisateur = sessionBean.getUser();
        }

        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;

        boolean droitsOk = true;


        String path = request.getRequestURI().substring(request.getContextPath().length());
        if (utilisateur != null) {

            if (path.contains("/user/")) {
                if (utilisateur.getAdmin()) {
                    droitsOk = false;
                }
            }
            if (path.contains("/admin/")) {
                if (!utilisateur.getAdmin()) {
                    droitsOk = false;
                }
            }

            if (droitsOk) { // on pousuit la requête
                chain.doFilter(req, res);
            } else { // oops, redirection
                response.sendRedirect(request.getContextPath() + "/forbidden.jsf");
            }
        } else {
            if (path.contains("connect")){
                chain.doFilter(req, res);
            } else {
                response.sendRedirect(request.getContextPath() + "/connect.jsf");
            }
        }


    }

    public void init(FilterConfig config) throws ServletException {
        // Nothing to do here!
    }

    public void destroy() {
        // Nothing to do here!
    }

}