package Bean;

import DAO.*;
import Model.Path;
import Model.Questionnaire;
import Model.Subject;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;

/**
 * Bean d'accès pour plusieurs quiz
 */
@Named
@ViewScoped
public class AllQuizBean implements Serializable {

    private static final long serialVersionUID = 4;
    private List<Questionnaire> allActiveQuiz;
    private List<Questionnaire> allInactiveQuiz;
    private QuestionnaireDao questionnaireDao;
    private QuestionDao questionDao;
    private AnswerDao answerDao;
    private SubjectDao subjectDao;
    private PathDao pathDao;

    @PostConstruct
    public void init(){
        // Initialisation des DAO utiles
        questionnaireDao = DAOFactory.getInstance().getQuestionnaireDao();
        questionDao = DAOFactory.getInstance().getQuestionDao();
        answerDao = DAOFactory.getInstance().getAnswerDao();
        subjectDao = DAOFactory.getInstance().getSubjectDao();
        pathDao = DAOFactory.getInstance().getPathDao();

        // Récupération des quiz actifs et inactifs
        allActiveQuiz = questionnaireDao.getByReq(questionnaireDao.activeQuiz);
        allInactiveQuiz = questionnaireDao.getByReq(questionnaireDao.inactiveQuiz);
    }

    /**
     * Inversion du status d'un questionnaire
     * @param questionnaire Questionnaire dont le status doit être inversé
     */
    public void toggleStatus(Questionnaire questionnaire){
        questionnaireDao.toggleStatus(questionnaire);
        allActiveQuiz = questionnaireDao.getByReq(questionnaireDao.activeQuiz);
        allInactiveQuiz = questionnaireDao.getByReq(questionnaireDao.inactiveQuiz);
    }

    /**
     * Récupération d'un sujet
     * @param questionnaire Questionnaire dont le sujet est souhaité
     * @return Nom du sujet
     */
    public String getSubject(Questionnaire questionnaire){
        Optional<Subject> subject = subjectDao.get(questionnaire.getSubjectId());
        if (subject.isPresent()){
            return subject.get().getLabel();
        } else {
            return "<N/A>";
        }
    }

    /**
     * Redirection vers la page d'affichage d'un quiz
     * @param id Id du quiz à afficher
     */
    public void show(long id){
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("showQuiz.jsf?id=" + id);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * Redirection vers la page de modification d'un quiz
     * @param id Id du quiz à modifier
     */
    public void modify(long id){
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("addQuiz.jsf?id=" + id);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Suppression d'un quiz
     * @param questionnaire Questionnaire à supprimer
     */
    public void delete(Questionnaire questionnaire){
        questionDao.getWithIdQuestionnaire(questionnaire.getId()).forEach(question -> {
            answerDao.getWithIdQuestion(question.getId()).forEach(answer -> {
                answerDao.delete(answer);
            });
            questionDao.delete(question);
        });
        questionnaireDao.delete(questionnaire);
        allActiveQuiz = questionnaireDao.getByReq(questionnaireDao.activeQuiz);
        allInactiveQuiz = questionnaireDao.getByReq(questionnaireDao.inactiveQuiz);
    }

    /**
     * Vérifie si un quiz contient au moins un parcours
     * @param id Id du quiz à vérifier
     * @return True si le quiz a au moins un parcours
     */
    public boolean hasPath(long id){
        List<Path> list = pathDao.getAllForQuiz(id);
        return !list.isEmpty();
    }

    /**
     * Redirection vers la page pour répondre à un quiz
     * @param id Id du quiz à passer
     */
    public void play(long id){
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("playQuiz.jsf?id=" + id);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // #####################
    // # Getters & Setters #
    // #####################

    public List<Questionnaire> getAllActiveQuiz() {
        return allActiveQuiz;
    }

    public void setAllActiveQuiz(List<Questionnaire> allActiveQuiz) {
        this.allActiveQuiz = allActiveQuiz;
    }

    public List<Questionnaire> getAllInactiveQuiz() {
        return allInactiveQuiz;
    }

    public void setAllInactiveQuiz(List<Questionnaire> allInactiveQuiz) {
        this.allInactiveQuiz = allInactiveQuiz;
    }
}
