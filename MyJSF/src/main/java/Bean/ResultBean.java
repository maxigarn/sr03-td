package Bean;

import DAO.*;
import Model.*;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.*;

/**
 * Bean des résultats
 */
@Named
@ViewScoped
public class ResultBean implements Serializable {

    private static final long serialVersionUID = 1;
    private Path path;
    private List<Subject> allSubjects;
    private Map<String,Object> mapSubjects;
    private Questionnaire questionnaire;
    private List<Question> allQuestions;
    private Map<Question,List<Answer>> reponses;
    private Subject newSubject;
    private List<Choice> allChoices;

    private PathDao pathDao;
    private ChoiceQADao choiceQADao;
    private SubjectDao subjectDao;
    private AnswerDao answerDao;
    private long quizId;
    private long pathId;

    private int currentIndex = 0;

    @PostConstruct
    public void init(){
        //init DAO
        choiceQADao = DAOFactory.getInstance().getChoiceQADao();
        answerDao = DAOFactory.getInstance().getAnswerDao();
        pathDao = DAOFactory.getInstance().getPathDao();

        allChoices = new ArrayList<>();
        reponses = new HashMap<>();
    }

    /**
     * Récupération de l'Id du parcours
     */
    public void onload() {
        allChoices = choiceQADao.getWithPath(pathId);
        allChoices.forEach(choice -> {
            reponses.put(choice.getQuestion(),answerDao.getWithIdQuestion(choice.getQuestion().getId()));
        });
        Optional<Path> optionalPath = pathDao.get(pathId);
        optionalPath.ifPresent(value -> path = value);
    }

    /**
     * Récupération de la durée du parcours
     * @return Le temps formaté en "00 min 00 sec"
     */
    public String getDuration(){
        int secondes = path.getDuration() % 60;
        int minutes = path.getDuration() / 60;
        String sec = String.valueOf(secondes) + " sec";
        String min = String.valueOf(minutes) + " min " ;
        if (secondes<10) {
            sec = "0" + sec;
        }
        if (minutes<10) {
            min = "0" + min;
        }

        return min + sec;
    }

    /**
     * Vérifie si une réponse à une question est correcte
     * @param question Question concernée
     * @param answer Réponse concernée
     * @return True -> correcte, False sinon
     */
    public boolean isCorrect(Question question,Answer answer){
        return question.getAnswerId() == answer.getPosition();
    }

    /**
     * Retourne les réponses d'une question
     * @param question Question concernée
     * @return Liste des réponses
     */
    public List<Answer> getAnswer(Question question){
        return reponses.get(question);
    }

    // #####################
    // # Getters & Setters #
    // #####################

    public List<Subject> getAllSubjects() {
        return allSubjects;
    }

    public void setAllSubjects(List<Subject> allSubjects) {
        this.allSubjects = allSubjects;
    }

    public Questionnaire getQuestionnaire() {
        return questionnaire;
    }

    public void setQuestionnaire(Questionnaire questionnaire) {
        this.questionnaire = questionnaire;
    }

    public List<Question> getAllQuestions() {
        return allQuestions;
    }

    public void setAllQuestions(List<Question> allQuestions) {
        this.allQuestions = allQuestions;
    }

    public Map<Question, List<Answer>> getReponses() {
        return reponses;
    }

    public void setReponses(Map<Question, List<Answer>> reponses) {
        this.reponses = reponses;
    }

    public SubjectDao getSubjectDao() {
        return subjectDao;
    }

    public void setSubjectDao(SubjectDao subjectDao) {
        this.subjectDao = subjectDao;
    }

    public Map<String, Object> getMapSubjects() {
        return mapSubjects;
    }

    public void setMapSubjects(Map<String, Object> mapSubjects) {
        this.mapSubjects = mapSubjects;
    }

    public Subject getNewSubject() {
        return newSubject;
    }

    public void setNewSubject(Subject newSubject) {
        this.newSubject = newSubject;
    }

    public long getQuizId() {
        return quizId;
    }

    public void setQuizId(long quizId) {
        this.quizId = quizId;
    }

    public int getCurrentIndex() {
        return currentIndex;
    }

    public void setCurrentIndex(int currentIndex) {
        this.currentIndex = currentIndex;
    }

    public void nextIndex(){
        setCurrentIndex(currentIndex+1);
    }

    public long getPathId() {
        return pathId;
    }

    public void setPathId(long pathId) {
        this.pathId = pathId;
    }

    public List<Choice> getAllChoices() {
        return allChoices;
    }

    public void setAllChoices(List<Choice> allChoices) {
        this.allChoices = allChoices;
    }

    public PathDao getPathDao() {
        return pathDao;
    }

    public void setPathDao(PathDao pathDao) {
        this.pathDao = pathDao;
    }

    public Path getPath() {
        return path;
    }

    public void setPath(Path path) {
        this.path = path;
    }
}
