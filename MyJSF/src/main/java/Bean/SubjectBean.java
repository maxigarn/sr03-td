package Bean;

import DAO.DAOFactory;
import DAO.SubjectDao;
import Model.Subject;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Bean des sujets
 */
@Named
@ViewScoped
public class SubjectBean implements Serializable {

    private static final long serialVersionUID = 1;
    private List<Subject> allSubjects;
    private Map<String,Object> mapSubjects;
    private Subject subject;
    private SubjectDao subjectDao;

    @PostConstruct
    public void init(){

        subject = new Subject();
        subjectDao = DAOFactory.getInstance().getSubjectDao();
        allSubjects = subjectDao.getAll();
        mapSubjects = new HashMap<>();
        allSubjects.forEach(s ->{
            mapSubjects.put(s.getLabel(), s.getId());
        });
    }

    /**
     * Ajout d'un sujet en base de données
     */
    public void addSubject(){
        subjectDao.save(subject);
        System.out.println("ADDING A THEME");
    }

    // #####################
    // # Getters & Setters #
    // #####################

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject= subject;
    }

    public List<Subject> getAllSubjects() {
        return allSubjects;
    }

    public void setAllSubjects(List<Subject> allSubjects) {
        this.allSubjects = allSubjects;
    }

    public SubjectDao getSubjectDao() {
        return subjectDao;
    }

    public void setSubjectDao(SubjectDao subjectDao) {
        this.subjectDao = subjectDao;
    }

    public Map<String, Object> getMapSubjects() {
        return mapSubjects;
    }

    public void setMapSubjects(Map<String, Object> mapSubjects) {
        this.mapSubjects = mapSubjects;
    }

}
