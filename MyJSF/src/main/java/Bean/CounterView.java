package Bean;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;

/**
 * Bean du compteur pour les quiz
 */
@Named
@ViewScoped
public class CounterView implements Serializable {

    private int number;

    @PostConstruct
    public void init(){
        System.out.println("INITALIZING COUNTER");
    }

    /**
     * Incrémentation du compteur
     */
    public void increment() {
        number++;
        System.out.println("INCREMENTING COUNTER VALUE TO : " + number);
    }

    @Override
    public String toString() {
        int secondes = number % 60;
        int minutes = number / 60;
        String sec = String.valueOf(secondes);
        String min = String.valueOf(minutes);
        if (secondes<10) {
            sec = "0" + sec;
        }
        if (minutes<10) {
            min = "0" + min;
        }

        return min + ":" + sec;
    }

    // #####################
    // # Getters & Setters #
    // #####################

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}