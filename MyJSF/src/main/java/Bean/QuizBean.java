package Bean;

import DAO.*;
import Model.*;
import net.bootsfaces.utils.FacesMessages;
import org.primefaces.PrimeFaces;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.*;

/**
 * Bean d'un questionnaire
 */
@Named
@ViewScoped
public class QuizBean implements Serializable {

    private static final long serialVersionUID = 1;
    private List<Subject> allSubjects;
    private Map<String,Object> mapSubjects;
    private Questionnaire questionnaire;
    private List<Question> allQuestions;
    private Map<Question,List<Answer>> reponses;
    private Subject newSubject;
    private SubjectDao subjectDao;
    private QuestionnaireDao questionnaireDao;
    private QuestionDao questionDao;
    private AnswerDao answerDao;
    private PathDao pathDao;
    private long quizId;

    private int currentIndex = 0;

    @PostConstruct
    public void init(){
        //init DAO
        subjectDao = DAOFactory.getInstance().getSubjectDao();
        questionnaireDao = DAOFactory.getInstance().getQuestionnaireDao();
        questionDao = DAOFactory.getInstance().getQuestionDao();
        answerDao = DAOFactory.getInstance().getAnswerDao();
        pathDao = DAOFactory.getInstance().getPathDao();

        questionnaire = new Questionnaire();
        allQuestions = new ArrayList<>();
        allSubjects = subjectDao.getAll();
        mapSubjects = new TreeMap<>();
        reponses = new LinkedHashMap<>();
        allSubjects.forEach(s ->{
            mapSubjects.put(s.getLabel(), s.getId());
        });
        newSubject = new Subject();
        addQuestion();
    }

    /**
     * Récupération de l'Id du questionnaire si nécessaire
     */
    public void onload() {

        if (!FacesContext.getCurrentInstance().isPostback() && quizId!=0) {
            Optional<Questionnaire> optionalQuestionnaire = questionnaireDao.get(quizId);
            optionalQuestionnaire.ifPresent(questionnaire1 -> {
                questionnaire = questionnaire1;
                allQuestions = questionDao.getWithIdQuestionnaire(questionnaire.getId());
                allQuestions.forEach(question -> {
                    reponses.put(question,answerDao.getWithIdQuestion(question.getId()));
                });
            });
        }
    }

    /**
     * Récupération du sujet d'un questionnaire
     * @return Titre du sujet
     */
    public String getSubject(){
        Optional<Subject> subject = subjectDao.get(questionnaire.getSubjectId());
        if (subject.isPresent()){
            return subject.get().getLabel();
        } else {
            return "<N/A>";
        }
    }

    /**
     * Remonte la question dans la liste
     * @param question Question concernée
     */
    public void upQuest(Question question){
        int idQuest = allQuestions.indexOf(question);
        Collections.swap(allQuestions, idQuest, idQuest-1);
    }

    /**
     * Descend la question dans la liste
     * @param question Question concernée
     */
    public void downQuest(Question question){
        int idQuest = allQuestions.indexOf(question);
        Collections.swap(allQuestions, idQuest, idQuest+1);
    }

    /**
     * Remonte la réponse dans la liste
     * @param question Question concernée
     * @param answer Réponse concernée
     */
    public void upAns(Question question, Answer answer){
        int idAns = reponses.get(question).indexOf(answer);
        Collections.swap(reponses.get(question), idAns, idAns-1);
    }

    /**
     * Descend la réponse dans la liste
     * @param question Question concernée
     * @param answer Réponse concernée
     */
    public void downAns(Question question, Answer answer){
        int idAns = reponses.get(question).indexOf(answer);
        Collections.swap(reponses.get(question), idAns, idAns+1);
    }

    /**
     * Récupère le pourcentage d'avancement suivant la question
     * @param question Question concernée
     * @return Pourcentage de quiz répondu
     */
    public String getPercent(Question question){
        double percent = (allQuestions.indexOf(question)+1);
        percent =  percent / allQuestions.size();
        percent *= 100;
        return String.valueOf(percent);
    }

    /**
     * Vérifie si c'est la bonne réponse
     * @param question Question concernée
     * @param answer Réponse concernée
     * @return True si c'est la bonne réponse, False sinon
     */
    public boolean isCorrect(Question question,Answer answer){
        if (question.getAnswerId()==answer.getPosition()){
            return true;
        } else {
            return false;
        }
    }

    /**
     * Récupération du sujet d'un questionnaire
     * @param questionnaire Questionnaire concerné
     * @return Titre du sujet
     */
    public String getSubject(Questionnaire questionnaire){
        Optional<Subject> subject = subjectDao.get(questionnaire.getSubjectId());
        if (subject.isPresent()){
            return subject.get().getLabel();
        } else {
            return "<N/A>";
        }
    }

    /**
     * Enregistre le questionnaire en base (Création ou Modification)
     */
    public void envoyerQuiz(){
        long idQuestionnaire;
        if (questionnaire.getId()==0) { // Le questionnaire doit être créé
            idQuestionnaire = questionnaireDao.save(questionnaire);
            FacesMessages.info("form:send","Info", "Questionnaire créé");
        } else{ // Le questionnaire doit être modifié
            idQuestionnaire = questionnaire.getId();
            questionnaireDao.update(questionnaire);
            FacesMessages.info("form:send","Info", "Questionnaire mis à jour");
        }
        allQuestions.forEach(question -> {
            long idQuestion;
            question.setQuestionnaireId(idQuestionnaire);
            question.setPosition(allQuestions.indexOf(question));

            if (question.getId()==0){ // La question doit être créée
                idQuestion = questionDao.save(question);
            } else { // La question doit être modifiée
                idQuestion = question.getId();
                questionDao.update(question);
            }

            reponses.get(question).forEach(answer -> {
                answer.setPosition(reponses.get(question).indexOf(answer));
                answer.setQuestionId(idQuestion);

                if (answer.getId()==0){ // La réponse doit être créée
                    answerDao.save(answer);
                } else { // La réponse doit être modifiée
                    answerDao.update(answer);
                }
            });
        });
    }

    /**
     * Ajout d'un thème
     */
    public void addTheme(){
        newSubject.setId(subjectDao.save(newSubject));
        mapSubjects.put(newSubject.getLabel(), newSubject.getId());
        PrimeFaces.current().executeScript("$('.modalPseudoClass').modal('hide');");
    }

    /**
     * Suppression d'une question
     * @param question Question concernée
     */
    public void removeQuestion(Question question){
        reponses.get(question).forEach(answer -> {
            if (answer.getId()!=0){
                // Suppression des réponses à la question qui existent déjà en base de données
                answerDao.delete(answer);
            }
        });

        reponses.remove(question);
        allQuestions.remove(question);

        if(question.getId()!=0){
            // Suppression de la question en base de données si elle existe
            questionDao.delete(question);
        }
    }

    /**
     * Suppression d'une réponse
     * @param question Question concernée
     * @param answer Réponse concernée
     */
    public void removeAnswer(Question question, Answer answer){
        reponses.get(question).forEach(ans -> {
            if (ans.getId()!=0){
                // Suppression de la réponse en base de données si elle existe
                answerDao.delete(ans);
            }
        });
        reponses.get(question).remove(answer);
    }

    /**
     * Ajout d'une question
     */
    public void addQuestion(){
        Question question = new Question();
        reponses.put(question,new ArrayList<>());
        reponses.get(question).add(new Answer());
        reponses.get(question).add(new Answer());
        allQuestions.add(question);
    }

    /**
     * Ajout d'une réponse à une question
     * @param question Question concernée
     */
    public void addAnswer(Question question){
        Answer answer = new Answer();
        reponses.get(question).add(answer);
    }


    /**
     * Récupération des parcours d'un quiz
     * @param quizId Questionnaire concerné
     * @return Liste des parcours
     */
    public List<Path> getAllPathsForQuiz(long quizId){
        return pathDao.getAllForQuiz(quizId);
    }

    /**
     * Récupération des réponses d'une question
     * @param question Question concernée
     * @return Liste des réponses
     */
    public List<Answer> getAnswer(Question question){
        return reponses.get(question);
    }


    // #####################
    // # Getters & Setters #
    // #####################

    public List<Subject> getAllSubjects() {
        return allSubjects;
    }

    public void setAllSubjects(List<Subject> allSubjects) {
        this.allSubjects = allSubjects;
    }

    public Questionnaire getQuestionnaire() {
        return questionnaire;
    }

    public void setQuestionnaire(Questionnaire questionnaire) {
        this.questionnaire = questionnaire;
    }

    public List<Question> getAllQuestions() {
        return allQuestions;
    }

    public void setAllQuestions(List<Question> allQuestions) {
        this.allQuestions = allQuestions;
    }

    public Map<Question, List<Answer>> getReponses() {
        return reponses;
    }

    public void setReponses(Map<Question, List<Answer>> reponses) {
        this.reponses = reponses;
    }

    public SubjectDao getSubjectDao() {
        return subjectDao;
    }

    public void setSubjectDao(SubjectDao subjectDao) {
        this.subjectDao = subjectDao;
    }

    public Map<String, Object> getMapSubjects() {
        return mapSubjects;
    }

    public void setMapSubjects(Map<String, Object> mapSubjects) {
        this.mapSubjects = mapSubjects;
    }

    public Subject getNewSubject() {
        return newSubject;
    }

    public void setNewSubject(Subject newSubject) {
        this.newSubject = newSubject;
    }

    public long getQuizId() {
        return quizId;
    }

    public void setQuizId(long quizId) {
        this.quizId = quizId;
    }

    public int getCurrentIndex() {
        return currentIndex;
    }

    public void setCurrentIndex(int currentIndex) {
        this.currentIndex = currentIndex;
    }

    public void nextIndex(){
        setCurrentIndex(currentIndex+1);
    }
}
