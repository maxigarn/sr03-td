package Bean;

import DAO.*;
import Model.*;
import net.bootsfaces.utils.FacesMessages;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Bean de gestion de parcours
 */
@Named
@ViewScoped
public class PathBean implements Serializable {

    private static final long serialVersionUID = 1;
    private Path path;
    private List<Choice> allChoices;
    private List<Subject> allSubjects;
    private Map<String,Object> mapSubjects;
    private Questionnaire questionnaire;
    private List<Question> allQuestions;
    private Map<Question,List<Answer>> responses;
    private User user;

    private PathDao pathDao;
    private ChoiceQADao choiceQADao;
    private SubjectDao subjectDao;
    private UserDao userDao;
    private QuestionnaireDao questionnaireDao;
    private long quizId;
    private long pathId;


    @PostConstruct
    public void init(){

        //init DAO
        pathDao = DAOFactory.getInstance().getPathDao();
        subjectDao = DAOFactory.getInstance().getSubjectDao();
        choiceQADao = DAOFactory.getInstance().getChoiceQADao();
        userDao = DAOFactory.getInstance().getUserDao();
        questionnaireDao = DAOFactory.getInstance().getQuestionnaireDao();

        path = new Path();
        allChoices = new ArrayList<>();
        questionnaire = new Questionnaire();
        allQuestions = new ArrayList<>();
        allSubjects = subjectDao.getAll();
        mapSubjects = new TreeMap<>();
        responses = new LinkedHashMap<>();
        allSubjects.forEach(s ->{
            mapSubjects.put(s.getLabel(), s.getId());
        });
        addQuestion();
    }


    /**
     * Récupération de l'id du parcours
     */
    public void onload() {

        if (!FacesContext.getCurrentInstance().isPostback() && pathId!=0) {
            Optional<Path> optionalPath = pathDao.get(quizId);
            optionalPath.ifPresent(path1 -> {
                path = path1;
            });
        }
    }

    /**
     * Récupération des parcours
     * @return Liste des parcours
     */
    public List<Path> getAllPaths(){
        return pathDao.getAll();
    }

    /**
     * Récupération des parcours d'un utilisateur
     * @param userId Id de l'utilisateur dont on souhaite les parcours
     * @return Liste des parcours
     */
    public List<Path> getAllPathsForUser(long userId){
        return pathDao.getAllForUser(userId);
    }

    /**
     * Enregistrement d'un parcours en base de données
     * @param duration Temps de réalisation du parcours
     */
    public void validerQuiz(int duration){

        // Enregistrement d'un parcours
        path.setQuestionnaireId(questionnaire.getId());
        path.setUserId(user.getId());
        path.setDuration(duration);
        int score = (int) allChoices.stream().filter(choice -> choice.getQuestion().getAnswerId() == choice.getAnswer().getPosition()).count();
        path.setScore(score);

        // Récupération de l'identifiant du parcours
        setPathId(pathDao.save(path));
        path.setId(pathId);

        // Informe l'utilisateur de la création du parcours
        FacesMessages.info("form:send","Info", "Parcours créé");


        allChoices.forEach(choice -> {

            System.out.println("Saving choice for question : " + choice.getQuestion());

            // Enregistrement des choix pour chaque question
            choice.setPathId(pathId);
            choiceQADao.save(choice);
        });

        try {
            // Redirection vers la page des résultats
            FacesContext.getCurrentInstance().getExternalContext().redirect("results.jsf?id=" + pathId);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Redirection vers la page d'affichage d'un parcours
     * @param id Id du parcours à afficher
     */
    public void show(long id){
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("results.jsf?id=" + id);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Ajout d'une question
     */
    public void addQuestion(){
        Question question = new Question();
        responses.put(question,new ArrayList<>());
        responses.get(question).add(new Answer());
        responses.get(question).add(new Answer());
        allQuestions.add(question);
    }

    /**
     * Ajout d'un reponse
     * @param question Question concernée
     */
    public void addAnswer(Question question){
        Answer answer = new Answer();
        responses.get(question).add(answer);
    }

    /**
     * Ajout d'un choix
     * @param question Question concernée
     * @param answer Réponse concernée
     */
    public void addChoice(Question question, Answer answer){
        Choice choice = new Choice();
        choice.setQuestion(question);
        choice.setAnswer(answer);
        allChoices.add(choice);
    }

    /**
     * Récupération de l'utilisateur par son Id
     * @param userId Id de l'utilisateur
     * @return Prénom et nom de l'utilisateur
     */
    public String getUser(long userId){
        Optional<User> user = userDao.get(userId);
        return user.map(value -> value.getFirstName() + " " + value.getLastName()).orElse("");
    }

    /**
     * Récupération d'un quiz par son Id
     * @param quizId Id du questionnaire
     * @return Nom du questionnaire
     */
    public String getQuizName(long quizId){
        Optional<Questionnaire> questionnaire = questionnaireDao.get(quizId);
        return questionnaire.map(Questionnaire::getNom).orElse("");
    }

    /**
     * Récupération d'un sujet par son Id
     * @param quizId Id du questionnaire
     * @return Titre du sujet
     */
    public String getSubjectName(long quizId){
        Optional<Questionnaire> questionnaire = questionnaireDao.get(quizId);
        if (questionnaire.isPresent()){
            Optional<Subject> subject = subjectDao.get(questionnaire.get().getSubjectId());
            return subject.map(Subject::getLabel).orElse("");
        }
        return "";
    }

    // #####################
    // # Getters & Setters #
    // #####################

    public List<Choice> getAllChoices() {
        return allChoices;
    }

    public void setAllChoices(List<Choice> allChoices) {
        this.allChoices = allChoices;
    }

    public List<Answer> getAnswer(Question question){
        return responses.get(question);
    }

    public Questionnaire getQuestionnaire() {
        return questionnaire;
    }

    public void setQuestionnaire(Questionnaire questionnaire) { this.questionnaire = questionnaire;  }

    public long getQuizId() {
        return quizId;
    }

    public void setQuizId(long quizId) {
        this.quizId = quizId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public long getPathId() {
        return pathId;
    }

    public void setPathId(long pathId) {
        this.pathId = pathId;
    }
}
