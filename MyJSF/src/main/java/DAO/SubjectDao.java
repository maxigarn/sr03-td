package DAO;

import Model.Subject;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * DAO des sujets
 */
public class SubjectDao implements Dao<Subject> {

    private DAOFactory dao;

    public SubjectDao(DAOFactory dao){
        this.dao = dao;
    }

    @Override
    public Optional<Subject> get(long id) {
        Subject subject = null;
        Connection connection = dao.getConnection();
        try {
            String sqlText = "SELECT * FROM sujet WHERE id=?;";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlText);
            preparedStatement.setLong(1, id);
            ResultSet res = preparedStatement.executeQuery();
            if (res.next()){
                subject = getSubjectFromResultSet(res);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Optional.ofNullable(subject);
    }

    @Override
    public List<Subject> getAll() {
        ArrayList<Subject> subjects = new ArrayList<>();
        Connection connection = dao.getConnection();
        try {
            String sqlText = "SELECT * FROM sujet;";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlText);
            ResultSet res = preparedStatement.executeQuery();
            while (res.next()){
                Subject subject = getSubjectFromResultSet(res);
                subjects.add(subject);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return subjects;
    }

    @Override
    public long save(Subject subject) {
        Connection connection = dao.getConnection();
        try {
            String sqlText = "INSERT INTO sujet (label) VALUES (?);";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlText, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, subject.getLabel());
            preparedStatement.executeUpdate();
            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
            if (generatedKeys.next()) {
                long l = generatedKeys.getLong(1);
                subject.setId(l);
                return l;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    public void update(Subject subject) {
        Connection connection = dao.getConnection();
        try {
            String sqlText = "UPDATE sujet SET label=? WHERE id=?;";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlText);
            preparedStatement.setString(1, subject.getLabel());
            preparedStatement.setLong(2, subject.getId());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Subject subject) {
        Connection connection = dao.getConnection();
        try{
            String sqlText = "DELETE FROM sujet WHERE id=?;";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlText);
            preparedStatement.setLong(1,subject.getId());
            preparedStatement.executeUpdate();
        }
        catch (SQLException e){
            e.printStackTrace();
        }
    }

    /**
     * Convertie un resultset en sujet
     * @param res Resultset
     * @return Sujet
     */
    public Subject getSubjectFromResultSet(ResultSet res) throws SQLException {
        Subject subject = new Subject();
        subject.setId(res.getInt("id"));
        subject.setLabel(res.getString("label"));
        return subject;
    }
}
