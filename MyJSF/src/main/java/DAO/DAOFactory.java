package DAO;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * DAO Factory utilisée pour la connexion en base de données
 */
public class DAOFactory {

    /**
     * URL de connection
     */
    private static String url = "jdbc:mysql://localhost:8889/projet-sr03";
    /**
     * Nom du user
     */
    private static String user = "root";
    /**
     * Mot de passe du user
     */
    private static String passwd = "root";
    /**
     * Objet Connection
     */
    private static Connection connect;

    private static DAOFactory myInstance;


    public static DAOFactory getInstance(){
        if (myInstance == null) {
            DAOFactory daoFactory = new DAOFactory();
            myInstance = daoFactory;
        }
        return myInstance;
    }

    /**
     * Méthode qui va nous retourner notre instance
     * et la créer si elle n'existe pas...
     * @return
     */
    public Connection getConnection(){
        if(connect == null){
            try {
                Class.forName( "com.mysql.jdbc.Driver" );
                connect = DriverManager.getConnection(url, user, passwd);
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        return connect;
    }

    /*
     * Méthodes de récupération de l'implémentation des différents DAO (un seul
     * pour le moment)
     */
    public UserDao getUserDao() {
        return new UserDao( this );
    }
    public SubjectDao getSubjectDao() {
        return new SubjectDao( this );
    }
    public QuestionnaireDao getQuestionnaireDao() {
        return new QuestionnaireDao( this );
    }
    public QuestionDao getQuestionDao() {
        return new QuestionDao( this );
    }
    public AnswerDao getAnswerDao() {
        return new AnswerDao( this );
    }
    public PathDao getPathDao() {
        return new PathDao( this );
    }
    public ChoiceQADao getChoiceQADao() {
        return new ChoiceQADao( this );
    }

}
