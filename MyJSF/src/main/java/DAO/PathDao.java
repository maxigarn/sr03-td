package DAO;

import Model.Path;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * DAO des parcours
 */
public class PathDao implements Dao<Path> {

    private DAOFactory dao;

    public PathDao(DAOFactory dao) {
        this.dao = dao;
    }

    @Override
    public Optional<Path> get(long id) {
        Path path = null;
        Connection connection = dao.getConnection();
        try{
            String sqlText = "SELECT * FROM parcours WHERE id=?;";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlText);
            preparedStatement.setLong(1,id);
            ResultSet res = preparedStatement.executeQuery();
            if (res.next()){
                path = getPathFromResultSet(res);
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        return Optional.ofNullable(path);
    }

    @Override
    public List<Path> getAll() {
        ArrayList<Path> paths = new ArrayList<>();
        Connection connection = dao.getConnection();
        try{
            String sqlText = "SELECT * FROM parcours;";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlText);
            ResultSet res = preparedStatement.executeQuery();
            while(res.next()){
                Path path = getPathFromResultSet(res);
                paths.add(path);
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        return paths;
    }

    /**
     * Récupère les parcours d'un utilisateur
     * @param userId Id d'un utilisateur
     * @return Liste des parcours
     */
    public List<Path> getAllForUser(long userId) {
        ArrayList<Path> paths = new ArrayList<>();
        Connection connection = dao.getConnection();
        try{
            String sqlText = "SELECT * FROM parcours WHERE idUser = ?;";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlText);
            preparedStatement.setLong(1, userId);
            ResultSet res = preparedStatement.executeQuery();
            while(res.next()){
                Path path = getPathFromResultSet(res);
                paths.add(path);
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        return paths;
    }


    /**
     * Récupère les parcours d'un quiz
     * @param quizId Id du quiz
     * @return Liste de parcours
     */
    public List<Path> getAllForQuiz(long quizId) {
        ArrayList<Path> paths = new ArrayList<>();
        Connection connection = dao.getConnection();
        try{
            String sqlText = "SELECT * FROM parcours WHERE idQuestionnaire = ?;";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlText);
            preparedStatement.setLong(1, quizId);
            ResultSet res = preparedStatement.executeQuery();
            while(res.next()){
                Path path = getPathFromResultSet(res);
                paths.add(path);
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        return paths;
    }

    public List<Path> getByReq(String sqlText){
        ArrayList<Path> paths = new ArrayList<>();
        Connection connection = dao.getConnection();
        try{
            PreparedStatement preparedStatement = connection.prepareStatement(sqlText);
            ResultSet res = preparedStatement.executeQuery();
            while(res.next()){
                Path path = getPathFromResultSet(res);
                paths.add(path);
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        return paths;
    }

    @Override
    public long save(Path path) {
        Connection connection = dao.getConnection();
        try {
            String sqlText = "INSERT INTO parcours (score, duree, idQuestionnaire, idUser, date) VALUES (?, ?, ?, ?, NOW());";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlText, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setInt(1, path.getScore());
            preparedStatement.setInt(2, path.getDuration());
            preparedStatement.setLong(3, path.getQuestionnaireId());
            preparedStatement.setLong(4, path.getUserId());
            preparedStatement.executeUpdate();
            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
            if (generatedKeys.next()) {
                long aLong = generatedKeys.getLong(1);
                path.setId(aLong);
                return aLong;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    public void update(Path path) {

    }

    @Override
    public void delete(Path path) {
        Connection connection = dao.getConnection();
        try{
            String sqlText = "DELETE FROM parcours WHERE id=?;";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlText);
            preparedStatement.setLong(1, path.getId());
            preparedStatement.executeUpdate();
        }
        catch (SQLException e){
            e.printStackTrace();
        }
    }

    /**
     * Convertie un resultset en parcours
     * @param res Résultset
     * @return Parcours
     */
    public Path getPathFromResultSet(ResultSet res) throws SQLException {
        Path path = new Path();
        path.setId(res.getInt("id"));
        path.setScore(res.getInt("score"));
        path.setDuration(res.getInt("duree"));
        path.setQuestionnaireId(res.getLong("idQuestionnaire"));
        path.setUserId(res.getLong("idUser"));
        path.setDate(res.getDate("date"));
        return path;
    }
}
