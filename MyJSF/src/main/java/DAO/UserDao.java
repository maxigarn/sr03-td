package DAO;

import Model.User;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * DAO des utilisateurs
 */
public class UserDao implements Dao<User> {

    private DAOFactory dao;
    public String sqlActiveUsers = "SELECT * FROM utilisateur WHERE status=1;";
    public String sqlInactiveUsers = "SELECT * FROM utilisateur WHERE status=0;";

    public UserDao(DAOFactory dao) {
        this.dao = dao;
    }

    @Override
    public Optional<User> get(long id){
        User usr = null;
        Connection connection = dao.getConnection();
        try{
            String sqlText = "SELECT * FROM utilisateur WHERE id = ? ";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlText);
            preparedStatement.setLong(1, id);
            ResultSet res = preparedStatement.executeQuery();
            if(res.next()){
                usr = getUserFromResultSet(res);
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        return Optional.ofNullable(usr);
    }

    /**
     * Vérifie si l'utilisateur existe en base de données lors de la connexion
     * @param mail Mail de connexion
     * @param password Mot de passe
     * @return Utilisateur si il existe
     */
    public User tryConnect(String mail, String password){
        User usr = null;
        Connection connection = dao.getConnection();
        String hashedPwd = hashMDP(password);
        try{
            String sqlText = "SELECT * FROM utilisateur WHERE adresseMail = ? AND password = ?;";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlText);
            preparedStatement.setString(1, mail);
            preparedStatement.setString(2, hashedPwd);
            ResultSet res = preparedStatement.executeQuery();
            if(res.next()){
                usr = getUserFromResultSet(res);
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        return usr;
    }

    @Override
    public List<User> getAll(){
        ArrayList<User> users = new ArrayList<>();
        Connection connection = dao.getConnection();
        try{
            String sqlText = "SELECT * FROM utilisateur;";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlText);
            ResultSet res = preparedStatement.executeQuery();
            while(res.next()){
                User usr = getUserFromResultSet(res);
                users.add(usr);
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        return users;
    }

    /**
     * Récupère les utilisateurs suivant une requête
     * @param sqlText Requête à exécuter
     * @return Liste des utilisateurs
     */
    public List<User> getByReq(String sqlText){
        ArrayList<User> users = new ArrayList<>();
        Connection connection = dao.getConnection();
        try{
            PreparedStatement preparedStatement = connection.prepareStatement(sqlText);
            ResultSet res = preparedStatement.executeQuery();
            while(res.next()){
                User usr = getUserFromResultSet(res);
                users.add(usr);
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        return users;
    }

    @Override
    public long save(User user){
        Connection connection = dao.getConnection();
        String mdp = hashMDP(user.getPassword());

        int status, admin;
        if (user.getStatus()){
            status = 1;
        } else {
            status = 0;
        }
        if (user.getAdmin()){
            admin = 1;
        } else {
            admin = 0;
        }

        try{
            String sqlText = "INSERT INTO utilisateur (adresseMail, password, name, firstName, company, phoneNumber,"
                    + " creationDate, status, admin) VALUES ( ? , ? , ? , ? , ? , ? , NOW(), ? , ? );";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlText);
            preparedStatement.setString(1, user.getEmail());
            preparedStatement.setString(2, mdp);
            preparedStatement.setString(3, user.getLastName());
            preparedStatement.setString(4, user.getFirstName());
            preparedStatement.setString(5, user.getCompany());
            preparedStatement.setString(6, user.getPhoneNumber());
            preparedStatement.setInt(7, status);
            preparedStatement.setInt(8, admin);

            preparedStatement.executeUpdate();
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public void update(User user){
        Connection connection = dao.getConnection();
        user.setPassword(hashMDP(user.getPassword()));
        try{
            String sqlText = "UPDATE utilisateur SET name=?, firstName=?, password=?, adresseMail=?, "
                    + "company=?, phoneNumber=?, admin=?, status=? WHERE id=?;";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlText);
            preparedStatement.setString(1, user.getLastName());
            preparedStatement.setString(2, user.getFirstName());
            preparedStatement.setString(3, user.getPassword());
            preparedStatement.setString(4, user.getEmail());
            preparedStatement.setString(5, user.getCompany());
            preparedStatement.setString(6, user.getPhoneNumber());
            preparedStatement.setInt(7, user.getAdmin()?1:0);
            preparedStatement.setInt(8, user.getStatus()?1:0);
            preparedStatement.setLong(9, user.getId());

            preparedStatement.executeUpdate();
        }
        catch (SQLException e){
            e.printStackTrace();
        }
    }

    /**
     * Inverse le status de l'utilisateur
     * @param user Utilisateur concerné
     */
    public void toggleStatus(User user){
        Connection connection = dao.getConnection();
        try {
            String sqlText = "UPDATE utilisateur SET status=? WHERE id=?";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlText);
            preparedStatement.setInt(1, user.getStatus() ? 0 : 1);
            preparedStatement.setLong(2, user.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e){
            e.printStackTrace();
        }

    }

    @Override
    public void delete(User user){
        Connection connection = dao.getConnection();
        try{
            String sqlText = "DELETE FROM utilisateur WHERE id=?;";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlText);
            preparedStatement.setLong(1,user.getId());
            preparedStatement.executeUpdate();
        }
        catch (SQLException e){
            e.printStackTrace();
        }

    }

    /**
     * Convertie un resultset en utilisateur
     * @param res Resultset
     * @return Utilisateur
     */
    public User getUserFromResultSet(ResultSet res) throws SQLException {
        User usr = new User();
        usr.setId(res.getInt("id"));
        usr.setEmail(res.getString("adresseMail"));
        usr.setPassword(res.getString("password"));
        usr.setFirstName(res.getString("firstName"));
        usr.setLastName(res.getString("name"));
        usr.setCompany(res.getString("company"));
        usr.setPhoneNumber(res.getString("phoneNumber"));
        usr.setCreationDate(res.getDate("creationDate"));
        if (res.getInt("status") == 0) {
            usr.setStatus(false);
        } else {
            usr.setStatus(true);
        }
        if (res.getInt("admin") == 0) {
            usr.setAdmin(false);
        } else {
            usr.setAdmin(true);
        }
        return usr;
    }

    private String hashMDP(String password){
        String mdp = password;
        if (mdp.length()<64) {
            MessageDigest digest = null;
            try {
                digest = MessageDigest.getInstance("SHA-256");
                byte[] hash = digest.digest(mdp.getBytes(StandardCharsets.UTF_8));
                mdp = toHexString(hash);
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        }
        return mdp;
    }

    public static String toHexString(byte[] bytes) {
        StringBuilder hexString = new StringBuilder();

        for (byte aByte : bytes) {
            String hex = Integer.toHexString(0xFF & aByte);
            if (hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }

        return hexString.toString();
    }

}