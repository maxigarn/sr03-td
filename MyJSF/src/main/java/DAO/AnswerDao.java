package DAO;

import Model.Answer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * DAO de réponse
 */
public class AnswerDao implements Dao<Answer> {

    private DAOFactory dao;

    public AnswerDao(DAOFactory dao) {
        this.dao = dao;
    }

    @Override
    public Optional<Answer> get(long id) {
        return Optional.empty();
    }

    @Override
    public List<Answer> getAll() {
        return null;
    }

    /**
     * Récupère les réponse suivant la question associée
     * @param id Id de la question
     * @return Liste de réponses
     */
    public List<Answer> getWithIdQuestion(long id){
        ArrayList<Answer> answers = new ArrayList<>();
        Connection connection = dao.getConnection();
        try{
            String sqlText = "SELECT * FROM reponse WHERE idQuest=? ORDER BY position;";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlText);
            preparedStatement.setLong(1,id);
            ResultSet res = preparedStatement.executeQuery();
            while(res.next()){
                Answer quest = getAnswerFromResultSet(res);
                answers.add(quest);
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        return answers;
    }

    @Override
    public long save(Answer answer) {
        Connection connection = dao.getConnection();
        try {
            String sqlText = "INSERT INTO reponse (texte, position, idQuest, status) VALUES (?,?,?,1);";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlText, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, answer.getEnonce());
            preparedStatement.setInt(2,answer.getPosition());
            preparedStatement.setLong(3,answer.getQuestionId());
            preparedStatement.executeUpdate();
            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
            if (generatedKeys.next()) {
                long aLong = generatedKeys.getLong(1);
                answer.setId(aLong);
                return aLong;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    public void update(Answer answer) {
        Connection connection = dao.getConnection();
        try {
            String sqlText = "UPDATE reponse SET texte=?, position=?, idQuest=?, status=? WHERE id=?;";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlText);
            preparedStatement.setString(1, answer.getEnonce());
            preparedStatement.setInt(2,answer.getPosition());
            preparedStatement.setLong(3,answer.getQuestionId());
            preparedStatement.setInt(4, answer.getStatus()?1:0);
            preparedStatement.setLong(5,answer.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Answer answer) {
        Connection connection = dao.getConnection();
        try{
            String sqlText = "DELETE FROM reponse WHERE id=?;";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlText);
            preparedStatement.setLong(1,answer.getId());
            preparedStatement.executeUpdate();
        }
        catch (SQLException e){
            e.printStackTrace();
        }
    }

    /**
     * Conversion d'un resultset en réponse
     * @param res ResultSet retourné par la base
     * @return Réponse
     */
    private Answer getAnswerFromResultSet(ResultSet res) throws SQLException {
        Answer answer = new Answer();
        answer.setId(res.getInt("id"));
        answer.setEnonce(res.getString("texte"));
        answer.setPosition(res.getInt("position"));
        answer.setQuestionId(res.getLong("idQuest"));
        if (res.getInt("status") == 0) {
            answer.setStatus(false);
        } else {
            answer.setStatus(true);
        }
        return answer;
    }
}
