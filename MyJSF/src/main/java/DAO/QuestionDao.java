package DAO;

import Model.Question;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * DAO des questions
 */
public class QuestionDao implements Dao<Question> {

    private DAOFactory dao;

    public QuestionDao(DAOFactory dao) {
        this.dao = dao;
    }

    @Override
    public Optional<Question> get(long id) {
        return Optional.empty();
    }

    @Override
    public List<Question> getAll() {
        return null;
    }

    /**
     * Récupère les questions d'un quiz
     * @param id Id du questionnaire
     * @return Liste de questions
     */
    public List<Question> getWithIdQuestionnaire(long id){
        ArrayList<Question> questions = new ArrayList<>();
        Connection connection = dao.getConnection();
        try{
            String sqlText = "SELECT * FROM question WHERE idQuestionnaire=? ORDER BY position;";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlText);
            preparedStatement.setLong(1,id);
            ResultSet res = preparedStatement.executeQuery();
            while(res.next()){
                Question quest = getQuestionFromResultSet(res);
                questions.add(quest);
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        return questions;
    }

    @Override
    public long save(Question question) {
        Connection connection = dao.getConnection();
        try {
            String sqlText = "INSERT INTO question (status, enonce, position, idRep, idQuestionnaire) VALUES (1,?,?,?,?);";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlText, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, question.getEnonce());
            preparedStatement.setInt(2,question.getPosition());
            preparedStatement.setLong(3,question.getAnswerId());
            preparedStatement.setLong(4,question.getQuestionnaireId());
            preparedStatement.executeUpdate();
            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
            if (generatedKeys.next()) {
                long aLong = generatedKeys.getLong(1);
                question.setId(aLong);
                return aLong;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    public void update(Question question) {

        Connection connection = dao.getConnection();
        try {
            String sqlText = "UPDATE question SET enonce=?, position=?, status=?, idQuestionnaire=?, idRep=? WHERE id=?;";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlText, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, question.getEnonce());
            preparedStatement.setInt(2,question.getPosition());
            preparedStatement.setInt(3,question.getStatus()?1:0);
            preparedStatement.setLong(4,question.getQuestionnaireId());
            preparedStatement.setLong(5,question.getAnswerId());
            preparedStatement.setLong(6,question.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Question question) {
        Connection connection = dao.getConnection();
        try{
            String sqlText = "DELETE FROM question WHERE id=?;";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlText);
            preparedStatement.setLong(1,question.getId());
            preparedStatement.executeUpdate();
        }
        catch (SQLException e){
            e.printStackTrace();
        }
    }

    /**
     * Convertie un resultset en question
     * @param res Resultset
     * @return Question
     */
    private Question getQuestionFromResultSet(ResultSet res) throws SQLException {
        Question quest = new Question();
        quest.setId(res.getInt("id"));
        quest.setEnonce(res.getString("enonce"));
        quest.setPosition(res.getInt("position"));
        quest.setQuestionnaireId(res.getLong("idQuestionnaire"));
        quest.setAnswerId(res.getLong("idRep"));
        if (res.getInt("status") == 0) {
            quest.setStatus(false);
        } else {
            quest.setStatus(true);
        }
        return quest;
    }
}
