package DAO;

import Model.Questionnaire;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * DAO des questionnaires
 */
public class QuestionnaireDao implements Dao<Questionnaire> {

    public String activeQuiz = "SELECT * FROM questionnaire WHERE status=1;";
    public String inactiveQuiz = "SELECT * FROM questionnaire WHERE status=0;";
    private DAOFactory dao;

    public QuestionnaireDao(DAOFactory dao) {
        this.dao = dao;
    }

    @Override
    public Optional<Questionnaire> get(long id) {
        Questionnaire questionnaire = null;
        Connection connection = dao.getConnection();
        try{
            String sqlText = "SELECT * FROM questionnaire WHERE id=?;";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlText);
            preparedStatement.setLong(1,id);
            ResultSet res = preparedStatement.executeQuery();
            if (res.next()){
                questionnaire = getQuestionnaireFromResultSet(res);
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        return Optional.ofNullable(questionnaire);
    }

    @Override
    public List<Questionnaire> getAll() {
        ArrayList<Questionnaire> questionnaires = new ArrayList<>();
        Connection connection = dao.getConnection();
        try{
            String sqlText = "SELECT * FROM questionnaire;";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlText);
            ResultSet res = preparedStatement.executeQuery();
            while(res.next()){
                Questionnaire quest = getQuestionnaireFromResultSet(res);
                questionnaires.add(quest);
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        return questionnaires;
    }

    /**
     * Récupère des questions suivant une requête
     * @param sqlText Requête à exécuter
     * @return Liste de questionnaires
     */
    public List<Questionnaire> getByReq(String sqlText){
        ArrayList<Questionnaire> questionnaires = new ArrayList<>();
        Connection connection = dao.getConnection();
        try{
            PreparedStatement preparedStatement = connection.prepareStatement(sqlText);
            ResultSet res = preparedStatement.executeQuery();
            while(res.next()){
                Questionnaire quest = getQuestionnaireFromResultSet(res);
                questionnaires.add(quest);
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        return questionnaires;
    }

    @Override
    public long save(Questionnaire questionnaire) {
        Connection connection = dao.getConnection();
        try {
            String sqlText = "INSERT INTO questionnaire (titre, status, idSujet) VALUES (?,1,?);";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlText, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, questionnaire.getNom());
            preparedStatement.setLong(2,questionnaire.getSubjectId());
            preparedStatement.executeUpdate();
            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
            if (generatedKeys.next()) {
                long aLong = generatedKeys.getLong(1);
                questionnaire.setId(aLong);
                return aLong;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    public void update(Questionnaire questionnaire) {
        Connection connection = dao.getConnection();
        try {
            String sqlText = "UPDATE questionnaire SET titre=?, status=?, idSujet=? WHERE id=?;";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlText);
            preparedStatement.setString(1, questionnaire.getNom());
            preparedStatement.setLong(2,questionnaire.getStatus()?1:0);
            preparedStatement.setLong(3,questionnaire.getSubjectId());
            preparedStatement.setLong(4,questionnaire.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Questionnaire questionnaire) {
        Connection connection = dao.getConnection();
        try{
            String sqlText = "DELETE FROM questionnaire WHERE id=?;";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlText);
            preparedStatement.setLong(1,questionnaire.getId());
            preparedStatement.executeUpdate();
        }
        catch (SQLException e){
            e.printStackTrace();
        }
    }

    /**
     * Inverse le status d'un questionnaire
     * @param questionnaire Questionnaire concerné
     */
    public void toggleStatus(Questionnaire questionnaire){
        Connection connection = dao.getConnection();
        try {
            String sqlText = "UPDATE questionnaire SET status=? WHERE id=?";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlText);
            preparedStatement.setInt(1, questionnaire.getStatus() ? 0 : 1);
            preparedStatement.setLong(2, questionnaire.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e){
            e.printStackTrace();
        }

    }

    /**
     * Convertie un resultset en questionnaire
     * @param res Resultset
     * @return Questionnaire
     */
    public Questionnaire getQuestionnaireFromResultSet(ResultSet res) throws SQLException {
        Questionnaire quest = new Questionnaire();
        quest.setId(res.getInt("id"));
        quest.setNom(res.getString("titre"));
        quest.setSubjectId(res.getLong("idSujet"));
        if (res.getInt("status") == 0) {
            quest.setStatus(false);
        } else {
            quest.setStatus(true);
        }
        return quest;
    }
}
