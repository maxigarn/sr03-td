package DAO;

import java.util.List;
import java.util.Optional;

/**
 * Interface du DAO
 * @param <T> Classe concernée par le DAO
 */
public interface Dao<T> {

    /**
     * Récupération suivant un Id
     * @param id Id à récupéré
     * @return Optional contenant l'objet ou null suivant si le record existe en base de données
     */
    Optional<T> get(long id);

    /**
     * Récupération de tous les records en base de données
     * @return Liste des objets
     */
    List<T> getAll();

    /**
     * Sauvegarde en base l'objet
     * @param t Objet à sauvegarder
     * @return Id de l'objet
     */
    long save(T t);

    /**
     * Modification de l'objet en base de données
     * @param t Objet à modifier
     */
    void update(T t);

    /**
     * Suppression de l'objet en base de données
     * @param t Objet à supprimer
     */
    void delete(T t);
}
