package DAO;

import Model.Answer;
import Model.Choice;
import Model.Question;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * DAO des choix
 */
public class ChoiceQADao implements Dao<Choice> {

    private DAOFactory dao;

    public ChoiceQADao(DAOFactory dao) {
        this.dao = dao;
    }

    @Override
    public Optional<Choice> get(long id) {
        Choice choice = null;
        Connection connection = dao.getConnection();
        try{
            String sqlText = "SELECT * FROM choix WHERE id=?;";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlText);
            preparedStatement.setLong(1,id);
            ResultSet res = preparedStatement.executeQuery();
            if (res.next()){
                choice = getPathFromResultSet(res);
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        return Optional.ofNullable(choice);
    }

    @Override
    public List<Choice> getAll() {
        ArrayList<Choice> choices = new ArrayList<>();
        Connection connection = dao.getConnection();
        try{
            String sqlText = "SELECT * FROM choix;";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlText);
            ResultSet res = preparedStatement.executeQuery();
            while(res.next()){
                Choice choice = getPathFromResultSet(res);
                choices.add(choice);
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        return choices;
    }

    /**
     * Récupération des choix associés à un parcours
     * @param pathId Id du parcours
     * @return Liste de choix
     */
    public List<Choice> getWithPath(long pathId) {
        ArrayList<Choice> choices = new ArrayList<>();
        Connection connection = dao.getConnection();
        try{
            String sqlText = "SELECT choix.id AS choiceId, idParcours, idQuestion, idReponse, question.id AS quesId, enonce, question.position AS quesPosition, idRep, idQuestionnaire, reponse.id AS repId, texte, reponse.position AS resPosition, idQuest FROM choix JOIN question ON choix.idQuestion = question.id JOIN reponse ON choix.idReponse = reponse.id WHERE choix.idParcours=?;";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlText);
            preparedStatement.setLong(1, pathId);
            ResultSet res = preparedStatement.executeQuery();
            while(res.next()){
                Choice choice = getPathFromResultSet(res);
                choices.add(choice);
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        return choices;
    }


    public List<Choice> getWithQuestions() {
        ArrayList<Choice> choices = new ArrayList<>();
        Connection connection = dao.getConnection();
        try{
            String sqlText = "SELECT * FROM choix JOIN question WHERE choix.idQuestion = question.id;";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlText);
            ResultSet res = preparedStatement.executeQuery();
            while(res.next()){
                Choice choice = getPathFromResultSet(res);
                choices.add(choice);
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        return choices;
    }

    /**
     * Récupération des choix suivant une requête fournit
     * @param sqlText Requête à exécuter
     * @return Liste de choix
     */
    public List<Choice> getByReq(String sqlText){
        ArrayList<Choice> choices = new ArrayList<>();
        Connection connection = dao.getConnection();
        try{
            PreparedStatement preparedStatement = connection.prepareStatement(sqlText);
            ResultSet res = preparedStatement.executeQuery();
            while(res.next()){
                Choice choice = getPathFromResultSet(res);
                choices.add(choice);
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        return choices;
    }

    @Override
    public long save(Choice choice) {
        Connection connection = dao.getConnection();
        try {
            String sqlText = "INSERT INTO choix (idParcours, idQuestion, idReponse) VALUES (?, ?, ?);";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlText, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setLong(1, choice.getPathId());
            preparedStatement.setLong(2, choice.getQuestion().getId());
            preparedStatement.setLong(3, choice.getAnswer().getId());
            preparedStatement.executeUpdate();
            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
            if (generatedKeys.next()) {
                long aLong = generatedKeys.getLong(1);
                choice.setId(aLong);
                return aLong;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    public void update(Choice choice) {

    }

    @Override
    public void delete(Choice choice) {
        Connection connection = dao.getConnection();
        try{
            String sqlText = "DELETE FROM choix WHERE id=?;";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlText);
            preparedStatement.setLong(1, choice.getId());
            preparedStatement.executeUpdate();
        }
        catch (SQLException e){
            e.printStackTrace();
        }
    }

    /**
     * Converti un resultset en choix
     * @param res Resultset
     * @return Choix
     */
    public Choice getPathFromResultSet(ResultSet res) throws SQLException {
        Choice choice = new Choice();
        Question question = new Question();
        Answer answer = new Answer();

        choice.setId(res.getInt("choiceId"));
        choice.setPathId(res.getLong("idParcours"));

        question.setId(res.getLong("quesId"));
        question.setEnonce(res.getString("enonce"));
        question.setPosition(res.getInt("quesPosition"));
        question.setAnswerId(res.getLong("IdRep"));
        question.setQuestionnaireId(res.getLong("idQuestionnaire"));

        answer.setId(res.getLong("repId"));
        answer.setEnonce(res.getString("texte"));
        answer.setPosition(res.getInt("resPosition"));
        answer.setQuestionId(res.getLong("idQuest"));

        choice.setQuestion(question);
        choice.setAnswer(answer);
        return choice;
    }
}
