package Model;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Utilisateur ayant un compte sur la plateforme
 * Peut se connecter à l'aide de son identifiant (email) et de son mot de passe (hashé dans la base de données)
 */
public class User {


    // #####################
    // #     Attributs     #
    // #####################

    /**
     * Id de l'utilisateur
     */
    private long id;
    /**
     * Mail
     */
    private String email;
    /**
     * Mot de passe
     */
    private String password;
    /**
     * Prénom
     */
    private String firstName;
    /**
     * Nom
     */
    private String lastName;
    /**
     * Entreprise
     */
    private String company;
    /**
     * Numéro de téléphone
     */
    private String phoneNumber;
    /**
     * Date de création
     */
    private Date creationDate;
    /**
     * status ==> utilisateur actif, peut se connecter || !status ==> utilisateur bloqué, obtiendra un message d'erreur lors d'une tentative de connexion
     */
    private Boolean status;
    /**
     * isAdmin==> administrateur, a accès à des fonctionnalités de gestion des utilisateurs (création, liste) et des questionnaires (création, accès) || !isAdmin==> utilisateur simple, n'a accès qu'aux questionnaires activés et aux résultats qu'il a obtenu par le passé
     */
    private Boolean isAdmin;

    @Override
    public String toString(){
        return "Nom: " + lastName
                + " <br>Prénom: " + firstName
                + " <br>Email: " + email
                + " <br>Mot de passe: " + password
                + " <br>Entreprise: " + company
                + " <br>Numéro tel: " + phoneNumber
                + "<br>Date de création: " + creationDate
                + ((isAdmin)?"<br>Adminstrateur":"<br>Utilisateur");
    }

    /**
     * Formatage de la date de création au format français
     * @return Date au format français (22 avr. 2019)
     */
    public String dateToFr(){
        return new SimpleDateFormat("dd MMM yyyy").format(this.creationDate);
    }

    // #####################
    // # Getters & Setters #
    // #####################

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Boolean getAdmin() {
        return isAdmin;
    }

    public void setAdmin(Boolean admin) {
        isAdmin = admin;
    }
}
