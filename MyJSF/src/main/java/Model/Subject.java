package Model;

/**
 * Sujet/Thème possible pour un questionnaire
 */
public class Subject {

    // #####################
    // #     Attributs     #
    // #####################

    /**
     * Id du sujet
     */
    private long id;
    /**
     * Label du sujet
     */
    private String label;

    @Override
    public String toString(){
        return "Sujet : " + this.label + " (id " + this.id + ")";
    }

    // #####################
    // # Getters & Setters #
    // #####################

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

}
