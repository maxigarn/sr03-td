package Model;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.util.Date;
import java.util.Properties;

/**
 * Classe permettant d'envoyer un mail.
 */

public class MailSender {
    private final static String MAILER_VERSION = "Java";

    public static void envoyerMailSMTP(String mail, String content, boolean debug) {
        //Envoi du fichier par mail
        InternetAddress[] internetAddresses = new InternetAddress[1];

        Properties prop = System.getProperties();
        prop.put("mail.smtp.host", "smtp1.utc.fr");
        prop.put("mail.smtp.port", "25");
        Session session = Session.getDefaultInstance(prop,null);
        Message message = new MimeMessage(session);
        try {
            internetAddresses[0] = new InternetAddress(mail);
            message.setFrom(new InternetAddress("ne_pas_repondre@utc.fr"));
            message.setRecipients(Message.RecipientType.TO,internetAddresses);

            // DESCRIPTION DE LA PIÈCE JOINTE

            //-----Partie Texte-------
            MimeBodyPart textBodyPart = new MimeBodyPart();
            textBodyPart.setText(content);
            //---je construit mon mail----
            MimeMultipart mimeMultipart = new MimeMultipart();
            mimeMultipart.addBodyPart(textBodyPart);
            //---------------------------------------
            message.setContent(mimeMultipart);
            message.setSubject("Plateforme Quiz - SR03");
            message.setHeader("X-Mailer", MAILER_VERSION);
            message.setSentDate(new Date());
            Transport.send(message);
        } catch (MessagingException e) {
            System.out.println(e);
        }
    }
}