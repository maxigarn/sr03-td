package Model;

/**
 * Classe représentant un choix effectué par un utilisateur lors d'un parcours (Path)
 * Associe une réponse (Answer) à une question (Question)
 */
public class Choice {

    // #####################
    // #     Attributs     #
    // #####################

    /**
     * Id du choix
     */
    private long id;
    /**
     * Id du parcours correspondant
     */
    private long pathId;
    /**
     * Question correspondante
     */
    private Question question;
    /**
     * Réponse correspondante
     */
    private Answer answer;

    @Override
    public String toString(){
        return "Question " + this.question + " de path " + this.pathId + " (réponse : " + this.answer + ")";
    }

    // #####################
    // # Getters & Setters #
    // #####################

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public Answer getAnswer() {
        return answer;
    }

    public void setAnswer(Answer answer) {
        this.answer = answer;
    }

    public long getPathId() {
        return pathId;
    }

    public void setPathId(long pathId) {
        this.pathId = pathId;
    }
}
