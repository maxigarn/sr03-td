package Model;

import java.util.Date;

/**
 * Parcours d'un questionnaire par un utilisateur
 */
public class Path {

    // #####################
    // #     Attributs     #
    // #####################

    /**
     * Id du parcours
     */
    private long id;
    /**
     * Id du Questionnaire
     */
    private long questionnaireId;
    /**
     * Id de l'utilisateur
     */
    private long userId;
    /**
     * Date à laquelle le parcours est réalisé
     */
    private Date date;
    /**
     * Durée du parcours (du chargement de la page playQuiz à la validation)
     */
    private int duration;
    /**
     * Score du parcours réalisé, en considérant un point par bonne réponse
     */
    private int score;

    @Override
    public String toString(){
        return "Path " + this.id + " par user" + this.userId+ " (effectué le : " + this.date + ")";
    }

    // #####################
    // # Getters & Setters #
    // #####################

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getQuestionnaireId() {
        return questionnaireId;
    }

    public void setQuestionnaireId(long questionnaireId) {
        this.questionnaireId = questionnaireId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
