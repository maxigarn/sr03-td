package Model;

/**
 * Questionnaire/Quiz réalisable par un utilisateur, associé à un thème (Subject)
 */
public class Questionnaire {

    // #####################
    // #     Attributs     #
    // #####################

    /**
     * Id du questionnaire
      */
    private long id;
    /**
     * Nom du questionnaire
     */
    private String nom;
    /**
     * Status du questionnaire
     */
    private Boolean status;
    /**
     * Id du sujet du questionnaire
     */
    private long subjectId;

    @Override
    public String toString(){
        return "Questionnaire " + this.id + " sur le sujet " + this.subjectId + (this.status? " [activé]" : " [désactivé]");
    }

    // #####################
    // # Getters & Setters #
    // #####################

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public long getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(long subjectId) {
        this.subjectId = subjectId;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
}
