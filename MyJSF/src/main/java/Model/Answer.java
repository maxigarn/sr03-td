package Model;

/**
 * Classe représentant une réponse à une question (Question)
 */
public class Answer {

    // #####################
    // #     Attributs     #
    // #####################

    /**
     * Id de la Réponse
     */
    private long id;
    /**
     * Status de la réponse
     */
    private Boolean status;
    /**
     * Texte de la réponse
     */
    private String enonce = "";
    /**
     * Id de la question correspondante
     */
    private long questionId;
    /**
     * Position de la réponse parmi les autres réponses possibles (pour une question donnée)
     */
    private int position;

    @Override
    public String toString(){
        return "Réponse " + this.id + " : " + this.enonce + " pour question " + this.questionId +  (this.status? " [activé]" : " [désactivé]");
    }

    // #####################
    // # Getters & Setters #
    // #####################

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getEnonce() {
        return enonce;
    }

    public void setEnonce(String enonce) {
        this.enonce = enonce;
    }

    public long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(long questionId) {
        this.questionId = questionId;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
