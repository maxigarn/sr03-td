package Model;

/**
 * Question associée à un questionnaire
 */
public class Question {

    // #####################
    // #     Attributs     #
    // #####################

    /**
     * Id de la question
     */
    private long id;
    /**
     * Status de la question
     */
    private Boolean status;
    /**
     * Enoncé de la question
     */
    private String enonce;
    /**
     * Numéro de la bonne réponse
     */
    private long answerId;
    /**
     * Id du questionnaire associé
     */
    private long questionnaireId;
    /**
     * Position de la question parmi les autres questions possibles (pour un questionnaire donné)
     */
    private int position;

    @Override
    public String toString(){
        return "Question " + this.id + " : " + this.enonce + " dans questionnaire " + this.questionnaireId + " (réponse : " + this.answerId + ") " + (this.status? "[activé]" : "[désactivé]");
    }

    // #####################
    // # Getters & Setters #
    // #####################

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getEnonce() {
        return enonce;
    }

    public void setEnonce(String enonce) {
        this.enonce = enonce;
    }

    public long getAnswerId() {
        return answerId;
    }

    public void setAnswerId(long answerId) {
        this.answerId = answerId;
    }

    public long getQuestionnaireId() {
        return questionnaireId;
    }

    public void setQuestionnaireId(long questionnaireId) {
        this.questionnaireId = questionnaireId;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
