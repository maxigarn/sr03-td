package REST;

import Model.Answer;
import Model.Question;

import java.util.List;

/**
 * Classe Question personnalisée pour le REST
 */
public class MyQuestion {

    // #####################
    // #     Attributs     #
    // #####################

    private long id;
    private String enonce;
    private long rightAnswer;
    private int position;
    private Boolean status;
    private List<Answer> answers;

    public MyQuestion(Question question, List<Answer> answers) {
        this.id = question.getId();
        this.status = question.getStatus();
        this.enonce = question.getEnonce();
        this.rightAnswer = question.getAnswerId();
        this.position = question.getPosition();
        this.answers = answers;
    }


    // #####################
    // # Getters & Setters #
    // #####################

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getEnonce() {
        return enonce;
    }

    public void setEnonce(String enonce) {
        this.enonce = enonce;
    }

    public long getRightAnswer() {
        return rightAnswer;
    }

    public void setRightAnswer(long rightAnswer) {
        this.rightAnswer = rightAnswer;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public List<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Answer> answers) {
        this.answers = answers;
    }
}
