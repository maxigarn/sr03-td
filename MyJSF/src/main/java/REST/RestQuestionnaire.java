package REST;

import DAO.DAOFactory;
import Model.Answer;
import Model.Question;
import Model.Questionnaire;
import Model.Subject;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Classe du REST permettant de retourner des questionnaires
 */
@Path("questionnaire")
public class RestQuestionnaire {

    /**
     * Récupération d'un questionnaire au format JSON
     * @param id Id du questionnaire
     * @return Questionnaire au format JSON
     */
    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getQuestionnaire(@PathParam("id") int id){
        Optional<Questionnaire> optionalQuestionnaire = DAOFactory.getInstance().getQuestionnaireDao().get(id);
        if (optionalQuestionnaire.isPresent()) {
            Questionnaire questionnaire = optionalQuestionnaire.get();
            Subject subject = DAOFactory.getInstance().getSubjectDao().get(questionnaire.getSubjectId()).get();
            List<MyQuestion> allQuestions = new ArrayList<>();
            List<Question> questions = DAOFactory.getInstance().getQuestionDao().getWithIdQuestionnaire(id);
            questions.forEach(question -> {
                List<Answer> answers = DAOFactory.getInstance().getAnswerDao().getWithIdQuestion(question.getId());
                MyQuestion myQuestion = new MyQuestion(question, answers);
                allQuestions.add(myQuestion);
            });
            MyQuestionnaire myQuestionnaire = new MyQuestionnaire(questionnaire, subject.getLabel(), allQuestions);

            return toJson(myQuestionnaire);
        } else {
            return "{\"error\" : \"Resource not found\"}";
        }
    }

    /**
     * Récupération de tous les questionnaires
     * @return Liste des questionnaires au format JSON
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getQuestionnaires(){
        List<Questionnaire> questionnaires = DAOFactory.getInstance().getQuestionnaireDao().getAll();
        List<MyQuestionnaire> myQuestionnaires = new ArrayList<>();
        questionnaires.forEach(questionnaire -> {
            Subject subject = DAOFactory.getInstance().getSubjectDao().get(questionnaire.getSubjectId()).get();
            List<MyQuestion> allQuestions = new ArrayList<>();
            List<Question> questions = DAOFactory.getInstance().getQuestionDao().getWithIdQuestionnaire(questionnaire.getId());
            questions.forEach(question -> {
                List<Answer> answers = DAOFactory.getInstance().getAnswerDao().getWithIdQuestion(question.getId());
                MyQuestion myQuestion = new MyQuestion(question, answers);
                allQuestions.add(myQuestion);
            });
            MyQuestionnaire myQuestionnaire = new MyQuestionnaire(questionnaire, subject.getLabel(), allQuestions);
            myQuestionnaires.add(myQuestionnaire);
        });

        return toJson(myQuestionnaires);
    }

    /**
     * Conversion d'un objet en JSON avec Jackson
     * @param obj
     * @param <T>
     * @return
     */
    public <T> String toJson(T obj){
        ObjectMapper mapper = new ObjectMapper();
        String s = "";
        try {
            s = mapper.writeValueAsString(obj);
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
        return s;
    }
}
