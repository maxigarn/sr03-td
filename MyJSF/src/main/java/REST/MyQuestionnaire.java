package REST;

import Model.Questionnaire;

import java.util.List;

/**
 * Classe Questionnaire personnalisée pour le REST
 */
public class MyQuestionnaire {

    // #####################
    // #     Attributs     #
    // #####################

    private long id;
    private String nom;
    private String subject;
    private Boolean isActive;
    private List<MyQuestion> questions;

    public MyQuestionnaire(Questionnaire questionnaire, String subject, List<MyQuestion> questions) {
        this.id = questionnaire.getId();
        this.nom = questionnaire.getNom();
        this.isActive = questionnaire.getStatus();
        this.subject = subject;
        this.questions = questions;
    }


    // #####################
    // # Getters & Setters #
    // #####################

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public List<MyQuestion> getQuestions() {
        return questions;
    }

    public void setQuestions(List<MyQuestion> questions) {
        this.questions = questions;
    }
}
