package Model;

public class Answer {
    private int id;
    private Boolean status;
    private String text;
    private int questionId;


    public Answer(int id, Boolean status, String text, int questionId) {
        this.id = id;
        this.status = status;
        this.text = text;
        this.questionId = questionId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public String toString(){
        return "Réponse " + this.id + " : " + this.text + " pour question " + this.questionId +  (this.status? " [activé]" : " [désactivé]");
    }

}
