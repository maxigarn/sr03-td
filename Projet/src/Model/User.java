package Model;

import java.util.Date;

public class User {
    private String email;
    private String password;
    private String firstName;
    private String lastName;
    private String company;
    private String phoneNumber;
    private Date creationDate;
    private Boolean status;
    private Boolean isAdmin;

    public User(String email, String password, String firstName, String lastName, String company, String phoneNumber, Date creationDate, Boolean status, Boolean isAdmin) {
        this.email = email;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.company = company;
        this.phoneNumber = phoneNumber;
        this.creationDate = creationDate;
        this.status = status;
        this.isAdmin = isAdmin;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Boolean getAdmin() {
        return isAdmin;
    }

    public void setAdmin(Boolean admin) {
        isAdmin = admin;
    }

    public String toString(){
        return "Nom: " + lastName + " <br>Prénom: " + firstName + " <br>Email: " + email + " <br>Mot de passe: " + password +
        " <br>Entreprise: " + company + " <br>Numéro tel: " + phoneNumber + "<br>Date de création: " + creationDate + ((isAdmin)?"<br>Adminstrateur":"<br>Utilisateur");
    }
}
