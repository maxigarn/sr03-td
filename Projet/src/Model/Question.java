package Model;

public class Question {
    private int id;
    private Boolean status;
    private String text;
    private int answerId;
    private int questionnaireId;


    public Question(int id, Boolean status, String text, int answerId, int questionnaireId) {
        this.id = id;
        this.status = status;
        this.text = text;
        this.answerId = answerId;
        this.questionnaireId = questionnaireId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getAnswerId() {
        return answerId;
    }

    public void setAnswerId(int answerId) {
        this.answerId = answerId;
    }

    public int getQuestionnaireId() {
        return questionnaireId;
    }

    public void setQuestionnaireId(int questionnaireId) {
        this.questionnaireId = questionnaireId;
    }

    public String toString(){
        return "Question " + this.id + " : " + this.text + " dans questionnaire " + this.questionnaireId + " (réponse : " + this.answerId + ") " + (this.status? "[activé]" : "[désactivé]");
    }

}
