package Model;

public class Subject {
    private int id;
    private String label;


    public Subject(int id, String label){
        this.id = id;
        this.label = label;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String toString(){
        return "Sujet : " + this.label + " (id " + this.id + ")";
    }
}
