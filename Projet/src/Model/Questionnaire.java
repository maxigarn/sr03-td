package Model;

public class Questionnaire {
    private int id;
    private Boolean status;
    private int subjectId;

    public Questionnaire(int id, Boolean status, int subjectId) {
        this.id = id;
        this.status = status;
        this.subjectId = subjectId;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public int getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(int subjectId) {
        this.subjectId = subjectId;
    }

    public String toString(){
        return "Questionnaire " + this.id + " sur le sujet " + this.subjectId + (this.status? " [activé]" : " [désactivé]");
    }
}
