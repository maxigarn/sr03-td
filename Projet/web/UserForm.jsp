<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Ajout d'un utilisateur</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>

<a href = "index.html">Retour</a><br>

<h2>Formulaire</h2>
<form action="create_user" method="post">
    <label>Prénom* </label>
    <input type="text" id="frname" name="user first name" required/>
    <br>
    <label>Nom* </label>
    <input type="text" id="faname" name="user familly name" required/>
    <br>
    <label>Email* </label>
    <input type="email" id="email" name="user email" required/>
    <br>
    <label>Entreprise </label>
    <input type="text" id="company" name="user company"/>
    <br>
    <label>Numéro de téléphone </label>
    <input type="text" id="phoneNumber" name="user phone number"/>
    <br>


    <label>Mot de passe* </label>
    <input type="password" id="psw" name="User password" required/>
    <br>
    <label>Administrateur ?</label>
    <label>Oui </label>
    <input type="radio" id="admin" name="isAdmin" value="admin"/>
    <label>Non </label>
    <input type="radio" id="user" name="isAdmin" value="user" checked/>
    <br>

    <label>* Champ obligatoire</label>


    <input type="submit" value="Submit">
</form>

</body>
</html>