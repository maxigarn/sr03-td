<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Ajout de réponse</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>

<a href = "index.html">Retour</a><br>

<h2>Formulaire d'ajout de réponse</h2>
<form action="create_answer" method="post">
    <label> ID de la réponse* </label>
    <input type="number" id="id" name="answer ID" required/>
    <br>
    <label>Intitulé de la réponse* </label>
    <input type="text" id="label" name="answer label" required/>
    <br>
    <label>ID de la question </label>
    <input type="text" id="id_parent" name="ID parent" required/>
    <br>

    <label>Activée ?</label>
    <label>Oui </label>
    <input type="radio" name="isActivated" value="yes"/>
    <label>Non </label>
    <input type="radio" name="isActivated" value="no" checked/>
    <br>

    <label>* Champ obligatoire</label>

    <br>
    <input type="submit" value="Submit">
</form>

</body>
</html>