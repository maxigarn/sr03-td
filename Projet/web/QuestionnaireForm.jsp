<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Ajout de questionnaire</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>

<a href = "index.html">Retour</a><br>

<h2>Formulaire d'ajout de questionnaire</h2>
<form action="create_questionnaire" method="post">
    <label> ID du questionnaire* </label>
    <input type="number" id="id" name="questionnaire ID" required/>
    <br>
    <label>Nom du questionnaire* </label>
    <input type="text" id="label" name="questionnaire label" required/>
    <br>
    <label>ID du sujet </label>
    <input type="text" id="id_parent" name="ID parent" required/>
    <br>

    <label>Activé ?</label>
    <label>Oui </label>
    <input type="radio" name="isActivated" value="yes"/>
    <label>Non </label>
    <input type="radio" name="isActivated" value="no" checked/>
    <br>

    <label>* Champ obligatoire</label>

    <br>
    <input type="submit" value="Submit">
</form>

</body>
</html>