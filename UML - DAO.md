# UML - DAO

*Alexandre TRUONG - Maxime GARNIER*

# Sommaire
[TOC]

# Enoncé
```
Comme j'ai précisé durant le dernier cours du SR03 vous devez 
rendre votre diagramme de classes jusqu'au 07/05 inclus.

Ce diagramme doit contenir la partie métier (toutes les classes 
que vous avez identifiées depuis le cahier des charges) + la 
partie couche donnée (classes qui implémentent l'accès aux 
données (CRUD) selon le design pattern DAO). 

Il est demandé également d'illustrer avec un seul exemple d'une 
de vos classes comment vous avez implémenté les opérations CRUD
(voir l'exemple du cours).
```

# UML

## Entier
![Diagramme entier](https://wwwetu.utc.fr/~maxigarn/UML_entier.svg)

## Partie Métier
![image alt](https://wwwetu.utc.fr/~maxigarn/UML_Metier.svg)

## Partie DAO
![image alt](https://wwwetu.utc.fr/~maxigarn/UML_DAO.svg)

# Couche donnée

## DAOs
* UserDAO
* SubjectDAO
* QuestionnaireDAO
* QuestionDAO
* AnswerDAO
* PathDAO

## Base de données MySQL

## Signature des méthodes des DAO

### DAO\<T>
```java
    // Récupération d'un objet par son id
    Optional<T> get(long id);
    // Récupération de tous les objets
    List<T> getAll();
    // Sauvgarde d'un objet
    long save(T t);
    // Mise à jour d'un objet
    long update(T t, String[] params);
    // Suppression d'un objet
    void delete(T t);
```


### UserDAO
```java
    // Récupération d'un objet par son id
    Optional<User> get(long id);
    // Récupération de tous les objets
    List<User> getAll();
    // Sauvgarde d'un objet
    long save(User user);
    // Mise à jour d'un objet
    long update(User user, String[] params);
    // Suppression d'un objet
    void delete(User user);
```

### SubjectDAO
```java
    // Récupération d'un objet par son id
    Optional<Subject> get(long id);
    // Récupération de tous les objets
    List<Subject> getAll();
    // Sauvgarde d'un objet
    long save(Subject subject);
    // Mise à jour d'un objet
    long update(Subject subject, String[] params);
    // Suppression d'un objet
    void delete(Subject subject);
```
### QuestionnaireDAO
```java
    // Récupération d'un objet par son id
    Optional<Questionnaire> get(long id);
    // Récupération de tous les objets
    List<Questionnaire> getAll();
    // Sauvgarde d'un objet
    long save(Questionnaire quest);
    // Mise à jour d'un objet
    long update(Questionnaire quest, String[] params);
    // Suppression d'un objet
    void delete(Questionnaire quest);
    // Récupère la liste des questions d'un questionnaire
    List<Question> getAllQuestions(idQuestionnaire:long);
    // Mise à jour de l'ordre des questions dans un questionnaire
    void updateQuestionsOrder(long idQuest, HashMap<long,int> mapOrdre);
```

### QuestionDAO
```java
    // Récupération d'un objet par son id
    Optional<Question> get(long id);
    // Récupération de tous les objets
    List<Question> getAll();
    // Sauvgarde d'un objet
    long save(Question quest);
    // Mise à jour d'un objet
    long update(Question quest, String[] params);
    // Suppression d'un objet
    void delete(Question quest);
    // Récupère la liste des réponses d'une question
    List<Response> getAllAnswers(long idQuestion);
    // Mise à jour de l'ordre des réponses d'une question
    void updateAnswersOrder(long idQuest, HashMap<long,int> mapOrder);
```

### AnswerDAO 
```java
    // Récupération d'un objet par son id
    Optional<Answer> get(long id);
    // Récupération de tous les objets
    List<Answer> getAll();
    // Sauvgarde d'un objet
    long save(Answer answer);
    // Mise à jour d'un objet
    long update(Answer answer, String[] params);
    // Suppression d'un objet
    void delete(Answer answer);
```

### HistoryDAO
```java
    // Récupération d'un objet par son id
    Optional<History> get(long id);
    // Récupération de tous les objets
    List<History> getAll();
    // Sauvgarde d'un objet
    long save(History history);
    // Mise à jour d'un objet
    long update(History history, String[] params);
    // Suppression d'un objet
    void delete(History history);
    // Retourne les couples des id question 
    // et id réponse choisies pour un parcours
    Hashmap<long,long> getAllChoices(long idHistory);
    // Ajout un choix dans un parcours
    long addChoice(long idHist, long idQuest, long idRep);
```

## Exemple de CRUD (User)

### Dao
```java
public interface Dao<T> {
     
    public Optional<T> get(long id);
     
    public List<T> getAll();
     
    public void save(T t);
     
    public void update(T t, String[] params);
     
    public void delete(T t);
}
```

### UserDao
```java
public class UserDao implements Dao<User>{

    private Connection db = null;
    private String databaseName = "???";
    private String databaseUser = "???";
    private String databasePwd = "???";

    public UserDAO(){
        db = DriverManager.getConnection("jdbc:mysql" 
            + databaseName, databaseUser, databasePwd);
    }

    @Override
    public Optional<User> get(long id){
        User usr = null;
	try{
		Statement sql = db.createStatement();
		String sqlText = "SELECT * IN USERS WHERE id=" + id + ";";
		ResultSet res = sql.executeQuery(sqlText);
		if(res){
			if(res.next()){
				usr = new User(res.getInt("id"), 
                                    res.getString("email"), 
                                    res.getString("password"), 
                                    res.getString("firstName"), 
                                    res.getString("lastName"), 
                                    res.getString("company"), 
                                    res.getString("phoneNumber"), 
                                    res.getDate("creationDate"), 
                                    res.getInt("status"));
			}
		}
	}
	catch (SQLException e){
		System.out.println(e);
	}
	return Optional.ofNullable(usr);

    }

    @Override
    public List<User> getAll(){
        ArraList<User> users = new ArrayList<User>;
	try{
		Statement sql = db.createStatement();
		String sqlText = "SELECT * IN USERS;";
		ResultSet res = sql.executeQuery(sqlText);
		if(res){
			while(res.next()){
				User usr = new User(res.getInt("id"), 
                                    res.getString("email"), 
                                    res.getString("password"), 
                                    res.getString("firstName"), 
                                    res.getString("lastName"), 
                                    res.getString("company"), 
                                    res.getString("phoneNumber"), 
                                    res.getDate("creationDate"), 
                                    res.getInt("status"));
				users.add(usr);
			}
		}
	}
	catch (SQLException e){
		System.out.println(e);
	}
	return users;

    }

    @Override
    public long save(User user){
        try{
		Statement sql = db.createStatement();
		String sqlText = "INSERT (email, password, firstName, lastName, "
                        + "company, phoneNumber, creationDate, status) "
                        + "IN USERS VALUES ('"+ user.getEmail + "', '"
                        + user.getPwd + "', '"+ user.getName + "', '"
                        + user.getCompany + "', '"+ user.getPhone 
                        + "', NOW(), '" + user.getStatus + "');"
		int nbUpdated = sql.executeUpdate(sqlText);
		if(nbUpdated){
			ResultSet keys = sql.getGeneratedKeys();
			if( keys.next() ) user.setId(keys.getLong(1));
                        return keys.getLong(1);
		}
	}
	catch (SQLException e){
		System.out.println(e);
	}

    }

    @Override
    public long update(User user, String[] params){
        boolean firstParam = true;
        try{
		Statement sql = db.createStatement();
		String sqlText = "UPDATE USERS SET ";
        for(String param : params){
            if(firstParam) { 
                firstParam = false;
            }
            else {
                sqlText += ", ";
            }
            switch (param) {
              case "email":
                sqlText += "email='" + user.getEmail() + "'";
                break;
              case "password":
                sqlText += "password='" + user.getPassword() + "'";
                break;
              case "firstName":
                sqlText += "firstName='" + user.getFirstName() + "'";
                break;
              case "lastName":
                sqlText += "lastName='" + user.getLastName() + "'";
                break;
              case "company":
                sqlText += "company='" + user.getCompany() + "'";
                break;
              case "phoneNumber":
                  sqlText += "phoneNumber='" + user.phoneNumber() + "'";
                break;
              case "creationDate":
                sqlText += "creationDate='" + user.getCreationDate() + "'";
                break;
              case "status":
                sqlText += "status='" + user.getStatus() + "'";
                break;
              default:
                break;
            }
        sqlText += "WHERE id=" + user.getId() + ";";
		sql.executeUpdate(sqlText);
	}
	catch (SQLException e){
		System.out.println(e);
	}

    }

    @Override
    public void delete(User user){
        try{
		Statement sql = db.createStatement();
		String sqlText = "DELETE FROM USERS WHERE id=" 
                            + user.getId + "';";
		sql.executeUpdate(sqlText);
	}
	catch (SQLException e){
		System.out.println(e);
	}

    }

}
```

### User
```java
public class User {
    // Attributs
    private long id;
    private String email;
    private String password;
    private String firstName;
    private String lastName;
    private String company;
    private String phoneNumber;
    private Date creationDate;
    private Boolean status;
    private Boolean isAdmin;

    // Constructeur
    public User(String email, String password, String firstName, 
    String lastName, String company, String phoneNumber, Date creationDate, 
    Boolean status, Boolean isAdmin) {
        this.email = email;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.company = company;
        this.phoneNumber = phoneNumber;
        this.creationDate = creationDate;
        this.status = status;
        this.isAdmin = isAdmin;
    }
    
    // Getters & Setters
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
    
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Boolean getAdmin() {
        return isAdmin;
    }

    public void setAdmin(Boolean admin) {
        isAdmin = admin;
    }

    // Surcharge de la méthode toString()
    public String toString(){
        return "Nom: " + lastName 
        + " <br>Prénom: " + firstName 
        + " <br>Email: " + email 
        + " <br>Mot de passe: " + password 
        + " <br>Entreprise: " + company 
        + " <br>Numéro tel: " + phoneNumber 
        + "<br>Date de création: " + creationDate 
        + ((isAdmin)?"<br>Adminstrateur":"<br>Utilisateur");
    }
}
```



###### tags: `SR03` `DAO`