public class Ex1 {

    private int[] tab;

    public void start(){

        tab = new int[10];

        for (int i = 0; i<10; i++){
            tab[i] = (int) (Math.random() * 100);
        }

        print();

        System.out.println("Minimum : " + min());
        System.out.println("Maximum : " + max());
        System.out.println("Moyenne : " + moyenne());
        System.out.println("Ecart Type : " + ecartType());

    }

    public void print(){
        for (int i = 0; i<10; i++){
            System.out.println("tab[" + i + "] : " + tab[i]);
        }
    }

    public int min(){
        int min = tab[0];
        for (int i = 0; i<10; i++){
            if(tab[i] < min) {
                min = tab[i];
            }
        }
        return min;
    }

    public int max(){
        int max = tab[0];
        for (int i = 0; i<10; i++){
            if(tab[i] > max) {
                max = tab[i];
            }
        }
        return max;
    }

    public double moyenne(){
        double moyenne = 0;
        for (int i = 0; i<10; i++){
            moyenne += tab[i];
        }
        moyenne /= 10;
        return moyenne;
    }

    public double ecartType(){

        double moy = moyenne();
        double var = 0;
        for (int i = 0; i<10; i++){
            var += (tab[i] - moy) * (tab[i] - moy);
        }
        var /= 9;
        return Math.sqrt(var);
    }


}
