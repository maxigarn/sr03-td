import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.Socket;
import java.util.Scanner;

public class ClientRestaurant {

    public static void main(String [] args){
        Socket comm;
        Point2D pt;
        ObjectOutputStream out;
        ObjectInputStream in;
        int x,y;
        Scanner sc = new Scanner(System.in);

        try {
            comm = new Socket("localhost",10080);
            System.out.println("Connexion réussie");
            System.out.println("Quel est votre position ?");
            System.out.print("X: ");
            x = sc.nextInt();
            System.out.print("Y: ");
            y = sc.nextInt();
            pt = new Point2D(x,y);
            out = new ObjectOutputStream(comm.getOutputStream());
            in = new ObjectInputStream(comm.getInputStream());
            out.writeObject(pt);
            Restaurant[] nearest = (Restaurant[]) in.readObject();
            System.out.println("Restaurants les plus proches : ");
            for (Restaurant r: nearest) {
                System.out.println(r);
            }
            comm.close();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
