import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class Client {

    public static void main(String[] args){

        Socket comm;

        try {
            comm = new Socket("localhost", 10080);
            System.out.println("Connexion client réussie");

            OutputStream out = comm.getOutputStream();
            out.write("Ceci est un message !".getBytes());


            //Reception de la réponse serveur
            InputStream in = comm.getInputStream();
            byte[] message = new byte[1024];
            in.read(message);
            System.out.println("Réponse : " + new String(message));

        } catch (IOException e) {
            e.printStackTrace();
        }

    }


}
