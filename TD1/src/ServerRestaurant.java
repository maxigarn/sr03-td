import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Arrays;

public class ServerRestaurant implements Serializable {

    public static void main(String [] args){
        ServerSocket conn;
        Socket comm;
        Point2D pt;
        ObjectInputStream in;
        ObjectOutputStream out;
        Restaurant[] nearest;

        Restaurant[] restaurants = new Restaurant[100];
        for(int i = 0; i<100;i++){
            restaurants[i] = new Restaurant();
            System.out.println("Restaurant " + (i+1) + " [" + restaurants[i] + "]");
        }
        //System.out.println("Restaurant le plus proche : " + nearestRestaurant(new Point2D(0,0),restaurants));

        try {
            conn = new ServerSocket(10080);
            comm = conn.accept();
            in = new ObjectInputStream(comm.getInputStream());
            out = new ObjectOutputStream(comm.getOutputStream());
            pt = (Point2D)in.readObject();
            System.out.println("Position du client : " + pt);
            nearest = nearestRestaurants(pt, restaurants);
            out.writeObject(nearest);
            conn.close();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private  static Restaurant[] nearestRestaurants(Point2D position, Restaurant[] restaurants){
        double[] tab;
        tab = new double[restaurants.length];
        double firstElement = Double.MAX_VALUE;
        double secondElement = Double.MAX_VALUE;
        double thirdElement = Double.MAX_VALUE;
        int[] nearest = new int[3];
        Restaurant[] r = new Restaurant[3];
        for (int i = 1;i<restaurants.length;i++){
            tab[i] = position.calculerDistance(restaurants[i].getPosition());
            if(tab[i]<firstElement){
                thirdElement = secondElement;
                secondElement = firstElement;
                firstElement = tab[i];
                nearest[0]=i;
            }else if(tab[i]<secondElement){
                thirdElement = secondElement;
                secondElement = tab[i];
                nearest[1]=i;
            }else if (tab[i]<thirdElement){
                thirdElement = tab[i];
                nearest[2] = i;
            }
        }
        r[0] = restaurants[nearest[0]];
        r[1] = restaurants[nearest[1]];
        r[2] = restaurants[nearest[2]];
        return r;
    }
}
