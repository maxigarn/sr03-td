import java.util.ArrayList;

public class Ex3 {

    private ArrayList<Point3D> tab;

    public void start(){
        tab = new ArrayList<Point3D>();
        tab.add(new Point3D());
        tab.add(new Point3D(2,3, 4));
        print();

        double dist = tab.get(0).calculerDistance(tab.get(1));
        System.out.println("Distance : " + dist);

    }

    public void print(){
        tab.forEach(pt -> {
            System.out.println(pt.toString());;
        });

    }

}
