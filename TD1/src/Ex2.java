import java.util.ArrayList;

public class Ex2 {

    private ArrayList<Point2D> tab;

    public void start(){
        tab = new ArrayList<Point2D>();
        tab.add(new Point2D());
        tab.add(new Point2D(2,3));
        print();

        double dist = tab.get(0).calculerDistance(tab.get(1));
        System.out.println("Distance : " + dist);

    }

    public void print(){
        tab.forEach(pt -> {
            pt.print();
        });

    }

}
