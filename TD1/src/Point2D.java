import java.io.Serializable;
import java.util.Random;

public class Point2D implements Serializable {

    protected int x;
    protected int y;

    public void setX(int x) {
        this.x = x;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }


    public Point2D (){
        Random random = new Random();
        setX(random.nextInt(1000));
        setY(random.nextInt(1000));
    }

    public Point2D (int x, int y){
        setX(x);
        setY(y);
    }

    public void print(){

        System.out.println("(" + getX() + ", " + getY() + ")");
    }

    public String toString(){
        return("(" + getX() + ", " + getY() + ")");
    }

    public double calculerDistance (Point2D pt){
        return Math.sqrt( (this.x - pt.x) * (this.x - pt.x) + (this.y - pt.y) * (this.y - pt.y) );
    }

}
