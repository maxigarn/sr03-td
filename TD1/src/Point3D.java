import java.util.Random;

public class Point3D extends Point2D{

    private int z;


    public int getZ() {
        return z;
    }

    public void setZ(int z) {
        this.z = z;
    }

    public Point3D (){
        super();
        Random random = new Random();
        setZ(random.nextInt(10000));
    }

    public Point3D (int x,  int y, int z){
        super(x, y);
        setZ(z);
    }

    public String toString(){
        return("(" + getX() + ", " + getY() + ", " + getZ() + ")");
    }

    public double calculerDistance (Point3D pt){
        return Math.sqrt( (this.x - pt.x) * (this.x - pt.x) + (this.y - pt.y) * (this.y - pt.y) + (this.z - pt.z) * (this.z - pt.z) );
    }

}
