import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Serveur {



    public static void main(String[] args){

        ServerSocket conn;
        Socket comm;


        try {
            conn = new ServerSocket(10080);

            while(true){
                try {
                    //Connexion
                    comm = conn.accept();
                    System.out.println("Nouvelle connexion (serveur)");

                    //Reception du message
                    InputStream in = comm.getInputStream();
                    byte[] message = new byte[1024];
                    in.read(message);
                    System.out.println("Message reçu : " + new String(message));

                    //Envoi d'une réponse
                    OutputStream out = comm.getOutputStream();
                    out.write("Réponse !".getBytes());

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }



    }

}
