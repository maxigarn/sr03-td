import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class ServeurAl {



    public static void main(String[] args){

        //Variables de co
        ServerSocket conn;
        Socket comm;

        //variables de jeu
        boolean playing = true;
        int nb_max_d=0; /*nbre d'allumettes maxi au départ*/
        int nb_allu_max=0; /*nbre d'allumettes maxi que l'on peut tirer au maxi*/
        int qui=0; /*qui joue? 0=Nous --- 1=PC*/
        int prise=0; /*nbre d'allumettes prises par le joueur*/
        int nb_allu_rest=0; /*nbre d'allumettes restantes*/


        try {
            conn = new ServerSocket(10080);

            //Reception des paramètres
            comm = conn.accept();
            System.out.println("Nouvelle connexion (serveur)");

            //Reception du message
            DataOutputStream out = new DataOutputStream(comm.getOutputStream());
            DataInputStream in = new DataInputStream(comm.getInputStream());
            nb_max_d = in.readInt();
            nb_allu_max = in.readInt();
            qui = in.readInt();
            nb_allu_rest = nb_max_d;

            do
            {
                if (qui==0)
                {
                    //Réception prise client
                    prise = in.readInt();
                    System.out.println("Réception prise client : " + prise);

                }
                else
                {
                    prise = jeu_ordi (nb_allu_rest , nb_allu_max);
                    out.writeInt(prise);
                }
                qui=(qui+1)%2;

                nb_allu_rest= nb_allu_rest - prise;
            }
            while (nb_allu_rest >0);

            conn.close();

        } catch (IOException e) {
            e.printStackTrace();
        }




    }

    public static int jeu_ordi (int nb_allum, int prise_max)
    {
        int prise = 0;
        int s = 0;
        float t = 0;
        s = prise_max + 1;
        t = ((float) (nb_allum - s)) / (prise_max + 1);
        while (t != Math.floor(t))
        {
            s--;
            t = ((float) (nb_allum-s)) / (prise_max + 1);
        }
        prise = s - 1;
        if (prise == 0)
            prise = 1;
        return (prise);
    }

}
