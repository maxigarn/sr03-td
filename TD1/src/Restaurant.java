import java.io.Serializable;
import java.util.Random;

public class Restaurant implements Serializable {
    private String name;
    private String phoneNumber;
    private Point2D position;

    public Restaurant(){
        Random random = new Random();
        name = "" + (char) (random.nextInt(26) + 'A') + (char) (random.nextInt(26) + 'a') + (char) (random.nextInt(26) + 'a')
                + (char) (random.nextInt(26) + 'a') + (char) (random.nextInt(26) + 'a')
                + (char) (random.nextInt(26) + 'a') + (char) (random.nextInt(26) + 'a')
                + (char) (random.nextInt(26) + 'a') + (char) (random.nextInt(26) + 'a')
                + (char) (random.nextInt(26) + 'a') + (char) (random.nextInt(26) + 'a');
        phoneNumber = "0" +
                Integer.toString(random.nextInt(9)+1) +
                Integer.toString(random.nextInt(10)) +
                Integer.toString(random.nextInt(10)) +
                Integer.toString(random.nextInt(10)) +
                Integer.toString(random.nextInt(10)) +
                Integer.toString(random.nextInt(10)) +
                Integer.toString(random.nextInt(10)) +
                Integer.toString(random.nextInt(10)) +
                Integer.toString(random.nextInt(10));
        position = new Point2D();
    }

    public Restaurant(String name, String phoneNumber, Point2D position){
        setName(name);
        setPhoneNumber(phoneNumber);
        setPosition(position);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Point2D getPosition() {
        return position;
    }

    public void setPosition(Point2D position) {
        this.position = position;
    }

    @Override
    public String toString() {
        return "Nom : " + getName() + " - Numéro de téléphone : " + getPhoneNumber() + " - Position : (" + getPosition().getX() + ";" + getPosition().getY() + ")";
    }
}
